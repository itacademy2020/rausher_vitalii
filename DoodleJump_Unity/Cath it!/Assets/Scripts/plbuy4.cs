﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class plbuy4 : MonoBehaviour
{
    public GameObject pl4;
    private void Start()
    {
        if (PlayerPrefs.GetInt("isBuy4") == 1)
        {
            Destroy(gameObject);
        }
    }
    private void FixedUpdate()
    {
        if ((transform.position.x < 2.07f && transform.position.y > -0.45f))
        {
            transform.position = new Vector2(transform.position.x + 0.1f, transform.position.y - 0.02f);
            transform.localScale = new Vector3(transform.localScale.x + 0.015f, transform.localScale.y + 0.015f, transform.localScale.z);
        }
        pl4.SetActive(false);
    }
    private void Update()
    {
        if (PlayerPrefs.GetInt("isBuy4") == 1)
        {
            Destroy(gameObject);
        }
    }
    private void OnMouseUp()
    {
        if (PlayerPrefs.GetInt("CCoin.s") > 249)
        {
            pl4.SetActive(true);
            PlayerPrefs.SetInt("CCoin.s", PlayerPrefs.GetInt("CCoin.s") - 250);
            PlayerPrefs.SetInt("isBuy4", 1);
            Destroy(gameObject);
        }
    }
}
