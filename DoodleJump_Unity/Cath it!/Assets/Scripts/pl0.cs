﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pl0 : MonoBehaviour
{
    public GameObject p0;
    private bool click;
    private void FixedUpdate()
    {
        if ((transform.position.x > -1.99f && transform.position.y > -0.55f))
        {
            transform.position = new Vector2(transform.position.x - 0.093f, transform.position.y - 0.03f);
            transform.localScale = new Vector3(transform.localScale.x + 0.015f, transform.localScale.y + 0.015f, transform.localScale.z);
        }
        if (PlatformSpawn.xxx == 0)
        {
            p0.SetActive(true);
        }
        else
        {
            p0.SetActive(false);
        }
    }
    private void OnMouseOver()
    {
        p0.SetActive(true);
        if (click)
        {
            PlatformSpawn.xxx = 0;
            click = false;
        }
    }
    private void OnMouseUp()
    {
        click = true;
    }
}
