﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movingscript : MonoBehaviour
{
    public static bool isMoving = false;
    public GameObject acx;
    public GameObject tach;
    private bool isSetting;

    private void FixedUpdate()
    {
        if (isMoving == true)
        {
            acx.SetActive(true);
            tach.SetActive(false);
        }   
        if (isMoving == false)
        {
            tach.SetActive(true);
            acx.SetActive(false);
        }
    }
}
