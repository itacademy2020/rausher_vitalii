﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackX : MonoBehaviour
{
    public GameObject back;
    public static bool isPl = false;
    private bool isBackX;

    private void Update()
    {
        isBackX = false;
    }
    private void OnMouseEnter()
    {
        back.SetActive(true);
    }
    private void OnMouseOver()
    {
        back.SetActive(true);
        if (isBackX)
        {
            SceneManager.LoadScene("main");
            isBackX = false;
        }
    }
    private void OnMouseExit()
    {
        back.SetActive(false);
    }
    private void OnMouseUp()
    {
        isBackX = true;
        isPl = true;
    }
}