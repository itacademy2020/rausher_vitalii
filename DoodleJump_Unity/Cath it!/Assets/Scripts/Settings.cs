﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Settings : MonoBehaviour
{
    private bool isRotate, isMove;

    public GameObject bk;

    public Quaternion rotation;

    private void FixedUpdate()
    {
        if ((transform.position.x > 0 && transform.position.y > 0))
        {
            bk.SetActive(true);
            transform.position = new Vector2(transform.position.x - 0.1f, transform.position.y - 0.2f);
            transform.localScale = new Vector3(transform.localScale.x + 0.015f, transform.localScale.y + 0.015f, transform.localScale.z);
            if (BackX.isPl == true)
            {
                transform.position = new Vector2(0, 0);
                transform.localScale = new Vector3(1.9f, 1.8f, transform.localScale.z);
                BackX.isPl = false;
            }
        }
       var mousePosition = Input.mousePosition;
       mousePosition.z = transform.position.z - Camera.main.transform.position.z; // это только для перспективной камеры необходимо
       mousePosition = Camera.main.ScreenToWorldPoint(mousePosition); //положение мыши из экранных в мировые координаты
       var angle = Vector2.Angle(Vector2.right, mousePosition - transform.position);//угол между вектором от объекта к мыше и осью х
       transform.eulerAngles = new Vector3(0f, 0f, transform.position.y < mousePosition.y ? angle : -angle);//немного магии на последок
    }
    private void OnMouseDown()
    {
        isMove = true;

    }
    private void OnMouseUp()
    {
    }
}