﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftMoveMenu : MonoBehaviour
{
    public Transform player;

    private void OnMouseDrag()
    {
        player.transform.localScale = new Vector3(-3.519126f, player.localScale.y, player.localScale.z);
    }
}