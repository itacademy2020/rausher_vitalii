﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSet : MonoBehaviour
{
    public GameObject set;
    private void Awake()
    {
        if (!BackSet.set)
        {
            Destroy(gameObject);
        }
        else
        {
            set.SetActive(true);
        }
    }
    private void Start()
    {

        if (BackSet.isBack == true)
        {
            transform.position = new Vector2(0, 0);
            transform.localScale = new Vector3(1.9f, 1.8f, transform.localScale.z);
        }

    }
    void FixedUpdate()
    {
        if ((transform.position.x < 2.01f && transform.position.y < 4.01f))
        {
            transform.position = new Vector2(transform.position.x + 0.1f, transform.position.y + 0.2f);
            transform.localScale = new Vector3(transform.localScale.x - 0.015f, transform.localScale.y - 0.015f, 1f);
        }
        if ((transform.position.x > 2f && transform.position.y > 4f))
        {
            set.SetActive(false);
            Destroy(gameObject);
        }
    }
}
