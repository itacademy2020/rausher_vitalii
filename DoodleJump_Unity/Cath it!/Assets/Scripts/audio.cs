﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class audio : MonoBehaviour
{
    public AudioClip music1;
    public AudioClip music2;
    public AudioClip musicgop;
    AudioSource audioSource;
    private void Awake()
    {
        GetComponent<AudioSource>().clip = music1;
        if (PlatformSpawn.xxx == 2)
        {
            GetComponent<AudioSource>().clip = music2;
        }
        if (gopnikstart.isGopnik == true)
        {
            GetComponent<AudioSource>().clip = musicgop;
        }
        if (Sound.isSoundOn)
        {
            GetComponent<AudioSource>().Play();
        }
    }
}
