﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Setting : MonoBehaviour
{
    public GameObject gb;

    private bool isSetting;

    private void Update()
    {
        isSetting = false;
    }
    private void OnMouseEnter()
    {
        gb.SetActive(true);
    }
    private void OnMouseOver()
    {
        gb.SetActive(true);
        if (isSetting)
        {
            SceneManager.LoadScene("settings");
            isSetting = false;
        }
    }
    private void OnMouseExit()
    {
        gb.SetActive(false);
    }
    private void OnMouseUp()
    {
        isSetting = true;
    }
}