﻿using UnityEngine;

public class PlayerMenu : MonoBehaviour
{
    public float horizontalSpeed = 0.1f;
    float speedX;
    public float verticalImpulse = 5f;
    Rigidbody2D rb;
    bool isGrounded;
    public float JumpForce = 10f;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        if (isGrounded)
        {
            rb.velocity = Vector2.up * JumpForce;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Platform")
        {
            isGrounded = true;
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Platform")
        {
            isGrounded = false;
        }

    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Platform")
        {
            isGrounded = false;
        }

    }
}
