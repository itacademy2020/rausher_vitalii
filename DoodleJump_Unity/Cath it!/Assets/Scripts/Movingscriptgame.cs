﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movingscriptgame : MonoBehaviour
{
    public GameObject left;
    public GameObject right;
    public Transform player;
    public Transform pl;
    public Transform plhell;
    public Transform plgop;
    [SerializeField]
    private float speed = 40f;
    Vector3 acc;
    // Start is called before the first frame update

    private void Awake()
    {
        player = pl;
        if (PlatformSpawn.xxx == 2)
        {
            player = plhell;
        }
        if (gopnikstart.isGopnik == true)
        {
            player = plgop;
        }
    }
    void Start()
    {
        if (Movingscript.isMoving == true)
        {
            left.SetActive(false);
            right.SetActive(false);
        }
    }
    private void FixedUpdate()
    {
        if (Movingscript.isMoving == true)
        {
            acc = Input.acceleration;
            player.position = new Vector2(player.position.x + acc.x * 1.5f * speed * Time.deltaTime, player.position.y);
            if (acc.x > 0 && gopnikstart.isGopnik == false && PlatformSpawn.xxx != 2)
            {
                player.transform.localScale = new Vector3(1.69924f, player.localScale.y, player.localScale.z);
            }
            if (acc.x < 0 && gopnikstart.isGopnik == false && PlatformSpawn.xxx != 2)
            {
                player.transform.localScale = new Vector3(-1.69924f, player.localScale.y, player.localScale.z);
            }
            if (acc.x > 0 && gopnikstart.isGopnik == true)
            {
                player.transform.localScale = new Vector3(0.2475516f, player.localScale.y, player.localScale.z);
            }
            if (acc.x < 0 && gopnikstart.isGopnik == true)
            {
                player.transform.localScale = new Vector3(-0.2475516f, player.localScale.y, player.localScale.z);
            }
            if (acc.x > 0 && PlatformSpawn.xxx == 2 && gopnikstart.isGopnik == false)
            {
                player.transform.localScale = new Vector3(0.63f, player.localScale.y, player.localScale.z);
            }
            if (acc.x < 0 && PlatformSpawn.xxx == 2 && gopnikstart.isGopnik == false)
            {
                player.transform.localScale = new Vector3(-0.63f, player.localScale.y, player.localScale.z);
            }
            if (player.position.x > 3.5f)
            {
                player.position = new Vector2(-3.5f, player.position.y);
            }
            if (player.position.x < -3.51)
            {
                player.position = new Vector2(3.49f, player.position.y);
            }
        }
    }

}
