﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Exit : MonoBehaviour
{
    public GameObject quit;

    private bool isExit;
    private void Update()
    {
        isExit = false;
    }
    private void OnMouseEnter()
    {
        quit.SetActive(true);
    }
    private void OnMouseOver()
    {
        quit.SetActive(true);
        if (isExit)
        {
            Application.Quit();
            isExit = false;
        }
    }
    private void OnMouseExit()
    {
        quit.SetActive(false);
    }
    private void OnMouseUp()
    {
        isExit = true;
    }
}
