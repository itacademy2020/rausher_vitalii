﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectPlatformMenu : MonoBehaviour
{
    public GameObject back2;
    public GameObject back2p;
    public GameObject back2p2;
    public GameObject back3;
    public GameObject back4;
    public GameObject back5;
    public GameObject back6;
    public GameObject pl;
    public GameObject pl0;
    public GameObject pl1;
    public GameObject pl2;
    public GameObject pl3;
    public GameObject pl4;
    public GameObject pl5;
    public GameObject pl6;
    public GameObject zamok;
    public static bool canStart;

    private void FixedUpdate()
    {
        if (SelecrRight.pl == 0)
        {
                zamok.SetActive(false);
                canStart = true;
                PlatformSpawn.xxx = 0;
            pl0.SetActive(true);
        }
        else 
        { 
            pl0.SetActive(false);
        }
        if (SelecrRight.pl == 1)
        {
            if (PlayerPrefs.GetInt("isBuy1") != 1)
            {
                zamok.SetActive(true);
                canStart = false;
            }
            else if (PlayerPrefs.GetInt("isBuy1") == 1)
            {
                zamok.SetActive(false);
                canStart = true;
                PlatformSpawn.xxx = 1;
            }
            pl1.SetActive(true);
            back3.SetActive(true);
        }
        else
        {
            pl1.SetActive(false);
            back3.SetActive(false);
        }
        if (SelecrRight.pl == 2)
        {
            if (PlayerPrefs.GetInt("isBuy2") != 1)
            {
                zamok.SetActive(true);
                canStart = false;
            }
            else if (PlayerPrefs.GetInt("isBuy2") == 1)
            {
                zamok.SetActive(false);
                canStart = true;
                PlatformSpawn.xxx = 2;
            }
            pl2.SetActive(true);
            back2.SetActive(true);
            back2p.SetActive(true);
            back2p2.SetActive(true);
        }
        else
        {
            pl2.SetActive(false);
            back2.SetActive(false);
            back2p.SetActive(false);
            back2p2.SetActive(false);
        }
        if (SelecrRight.pl == 3)
        {
            if (PlayerPrefs.GetInt("isBuy3") != 1)
            {
                zamok.SetActive(true);
                canStart = false;
            }
            else if (PlayerPrefs.GetInt("isBuy3") == 1)
            {
                zamok.SetActive(false);
                canStart = true;
                PlatformSpawn.xxx = 3;
            }
            pl3.SetActive(true);
        }
        else
        {
            pl3.SetActive(false);
        }
        if (SelecrRight.pl == 4)
        {
            if (PlayerPrefs.GetInt("isBuy4") != 1)
            {
                zamok.SetActive(true);
                canStart = false;

            }
            else if (PlayerPrefs.GetInt("isBuy4") == 1)
            {
                zamok.SetActive(false);
                canStart = true;
                PlatformSpawn.xxx = 4;
            }
            pl4.SetActive(true);
            back4.SetActive(true);
        }
        else
        {
            pl4.SetActive(false);
            back4.SetActive(false);
        }
        if (SelecrRight.pl == 5)
        {
            if (PlayerPrefs.GetInt("isBuy5") != 1)
            {
                zamok.SetActive(true);
                canStart = false;
            }
            else if (PlayerPrefs.GetInt("isBuy5") == 1)
            {
                zamok.SetActive(false);
                canStart = true;
                PlatformSpawn.xxx = 5;
            }
            pl5.SetActive(true);
            back5.SetActive(true);
        }
        else
        {
            pl5.SetActive(false);
            back5.SetActive(false);
        }
        if (SelecrRight.pl == 6)
        {
            if (PlayerPrefs.GetInt("isBuy6") != 1)
            {
                zamok.SetActive(true);
                canStart = false;
            }
            else if (PlayerPrefs.GetInt("isBuy6") == 1)
            {
                zamok.SetActive(false);
                canStart = true;
                PlatformSpawn.xxx = 6;
            }
            pl6.SetActive(true);
            back6.SetActive(true);
        }
        else
        {
            pl6.SetActive(false);
            back6.SetActive(false);
        }
    }
}
