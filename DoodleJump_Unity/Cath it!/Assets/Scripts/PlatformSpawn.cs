﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;


    public class PlatformSpawn : MonoBehaviour
{
    public GameObject back2;
    public GameObject back2p;
    public GameObject back2p2;
    public GameObject back3;
    public GameObject back4;
    public GameObject back5;
    public GameObject back6;
    public GameObject pltf;
    public GameObject pltfgop;
    public  GameObject platform;
    public  GameObject platform1;
    public  GameObject platform2;
    public  GameObject platform3;
    public  GameObject platform4;
    public  GameObject platform5;
    public  GameObject platform6;
    public  GameObject platform7;
    public static int xxx = 0;

    public GameObject gop;
    public GameObject it;
    public GameObject nazar;
    public GameObject plgop;
    public GameObject pl;
    public GameObject plhell;
    public GameObject player;
    private float x,xx;
    
    AudioSource audioSource;

    private void Awake()
    {
        if (gopnikstart.isGopnik == true)
        {
            player = plgop;
            plgop.SetActive(true);
            pl.SetActive(false);
            gop.SetActive(true);
            it.SetActive(true);
            nazar.SetActive(true);
            if (Player.lose)
            {
                nazar.SetActive(false);
            }

        }
        if (gopnikstart.isGopnik == false)
        {
            player = pl;
            plgop.SetActive(false);
            pl.SetActive(true);
            it.SetActive(false);
            nazar.SetActive(false);
            gop.SetActive(false);
        }
        player = pl;
        if (xxx == 0)
        {
            platform = pltf;
        }
        if (xxx == 1)
        {
            platform = platform1;
        }
        if (xxx == 2)
        {
            platform = platform2;
            player = plhell;
            pl.SetActive(false);
            plhell.SetActive(true);
        }
        if (xxx == 3)
        {
            platform = platform3;
        }
        if (xxx == 4)
        {
            platform = platform4;
        }
        if (xxx == 5)
        {
            platform = platform5;
        }
        if (xxx == 6)
        {
            platform = platform6;
        }
        if (gopnikstart.isGopnik == true)
        {
            platform = pltfgop;
            player = plgop;
            plhell.SetActive(false);

        }
    }
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        StartCoroutine(Spawn());
    }
    private void FixedUpdate()
    {
        transform.Translate(0, 0.01f, 0);
        x = player.transform.position.y * 10f;
        if (xxx == 1)
        {
            back3.SetActive(true);
        }
        if (xxx == 2)
        {
            back2.SetActive(true);
            back2p.SetActive(true);
            back2p2.SetActive(true);
        }
        if (xxx == 4)
        {
            back4.SetActive(true);
        }
        if (xxx == 5)
        {
            back5.SetActive(true);
        }
        if (xxx == 6)
        {
            back6.SetActive(true);
        }
    }

    IEnumerator Spawn() 
    {
        while (true)
        {
            if (transform.position.y + 1.6f < player.transform.position.y)
            {
                Instantiate(platform, new Vector2(Random.Range(-2f, 2f), transform.position.y + 6f), Quaternion.identity);
            }
            if (transform.position.y - 0.7f < player.transform.position.y)
            {
                Instantiate(platform, new Vector2(Random.Range(-2f, 2f), transform.position.y + 9.5f), Quaternion.identity);
                yield return new WaitForSeconds(0.3f);
            }
            else
            {
                Instantiate(platform, new Vector2(Random.Range(-2f, 2f), transform.position.y + 11f), Quaternion.identity);
                yield return new WaitForSeconds(0.3f);
            }
        }
    }
}