﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Back : MonoBehaviour
{
    public GameObject back;
    public GameObject set;

    private bool isBack;

    private void Update()
    {
        isBack = false;
    }

    private void OnMouseEnter()
    {
        back.SetActive(true);
    }

    private void OnMouseOver()
    {
        back.SetActive(true);
        if (isBack)
        {
            BackSet.set = false;
            PlayerPrefs.SetInt("CCoin.s", CoinsCollect.collectC / 2);
            SceneManager.LoadScene("main");
            isBack = false;

        }
    }

    private void OnMouseExit()
    {
        back.SetActive(false);
    }
    void OnMouseUp()
    {
        isBack = true;
    }
}
