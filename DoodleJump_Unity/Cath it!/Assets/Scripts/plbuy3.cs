﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class plbuy3 : MonoBehaviour
{
    public GameObject pl3;
    private void Start()
    {
        if (PlayerPrefs.GetInt("isBuy3") == 1)
        {
            Destroy(gameObject);
        }
    }
    private void FixedUpdate()
    {
        if ((transform.position.x < 1.82f && transform.position.y < 1.05f))
        {
            transform.position = new Vector2(transform.position.x + 0.085f, transform.position.y + 0.05f);
            transform.localScale = new Vector3(transform.localScale.x + 0.015f, transform.localScale.y + 0.015f, transform.localScale.z);
        }
        pl3.SetActive(false);
    }
    private void Update()
    {
        if (PlayerPrefs.GetInt("isBuy3") == 1)
        {
            Destroy(gameObject);
        }
    }
    private void OnMouseUp()
    {
        if (PlayerPrefs.GetInt("CCoin.s") > 199)
        {
            pl3.SetActive(true);
            PlayerPrefs.SetInt("CCoin.s", PlayerPrefs.GetInt("CCoin.s") - 200);
            PlayerPrefs.SetInt("isBuy3", 1);
            Destroy(gameObject);
        }
    }
}
