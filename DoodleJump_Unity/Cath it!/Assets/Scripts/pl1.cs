﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pl1 : MonoBehaviour
{
    public GameObject p1;
    private bool click;
    private void FixedUpdate()
    {
        if ((transform.position.x > -1.46f && transform.position.y < 1.05f))
        {
            transform.position = new Vector2(transform.position.x - 0.085f, transform.position.y + 0.055f);
            transform.localScale = new Vector3(transform.localScale.x + 0.015f, transform.localScale.y + 0.015f, transform.localScale.z);
        }
        if (PlatformSpawn.xxx == 1)
        {
            p1.SetActive(true);
        }
        else
        {
            p1.SetActive(false);
        }
    }
    private void OnMouseOver()
    {
        p1.SetActive(true);
        if (click)
        {
            PlatformSpawn.xxx = 1;
            click = false;
        }
    }
    private void OnMouseUp()
    {
        click = true;
    }
}
