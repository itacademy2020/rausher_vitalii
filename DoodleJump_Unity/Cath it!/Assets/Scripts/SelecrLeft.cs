﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SelecrLeft : MonoBehaviour
{
    public GameObject select;
    public static bool isLeftMove;
    private bool isSelect;
    private void Update()
    {
        isSelect = false;
        isLeftMove = false;
    }
    private void OnMouseEnter()
    {
        select.SetActive(true);
    }
    private void OnMouseOver()
    {
        select.SetActive(true);
        if (isSelect)
        {
            SelecrRight.pl--;
            if (SelecrRight.pl == -1)
            {
                SelecrRight.pl = 6;
            }
        }
    }
    private void OnMouseExit()
    {
        select.SetActive(false);
    }
    private void OnMouseUp()
    {
        isSelect = true;
    }
}