﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Platforms : MonoBehaviour
{
    public GameObject gb;
    private bool bl;

    private void FixedUpdate()
    {
        bl = false;
    }
    private void OnMouseEnter()
    {
        gb.SetActive(true);
    }
    private void OnMouseOver()
    {
        gb.SetActive(true);
        if (bl)
        {
            SceneManager.LoadScene("platforms");
            bl = false;
        }
    }
    private void OnMouseExit()
    {
        gb.SetActive(false);
    }
    private void OnMouseUp()
    {
        bl = true;
    }
}