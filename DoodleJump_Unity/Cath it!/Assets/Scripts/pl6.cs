﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pl6 : MonoBehaviour
{
    public GameObject p2;
    private bool click;
    private void FixedUpdate()
    {
        if ((transform.position.x > -1f && transform.position.y > -1.85f))
        {
            transform.position = new Vector2(transform.position.x - 0.05f, transform.position.y - 0.093f);
            transform.localScale = new Vector3(transform.localScale.x + 0.015f, transform.localScale.y + 0.015f, transform.localScale.z);
        }
        if (PlatformSpawn.xxx == 6)
        {
            p2.SetActive(true);
        }
        else
        {
            p2.SetActive(false);
        }
    }
    private void OnMouseOver()
    {
        p2.SetActive(true);
        if (click)
        {
            PlatformSpawn.xxx = 6;
            click = false;
        }
    }
    private void OnMouseUp()
    {
        click = true;
    }
}