﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundOff : MonoBehaviour
{
    public GameObject sounOff;

    private void FixedUpdate()
    {
        if (!Sound.isSoundOn)
        {
            sounOff.SetActive(true);
        }
        else
        {
            sounOff.SetActive(false);
        }
    }
    private void OnMouseEnter()
    {
        if (!Sound.isSoundOn)
        {
            sounOff.SetActive(true);
        }
        else
        {
            sounOff.SetActive(false);
        }
    }
    private void OnMouseOver()
    {
        if (!Sound.isSoundOn)
        {
            sounOff.SetActive(true);
        }
        else
        {
            sounOff.SetActive(false);
        }
    }
    private void OnMouseExit()
    {
        if (!Sound.isSoundOn)
        {
            sounOff.SetActive(true);
        }
        else
        {
            sounOff.SetActive(false);
        }
    }
    private void OnMouseUp()
    {
        Sound.isSoundOn = false;
    }
}
