﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class plbuy1 : MonoBehaviour
{
    public GameObject pl1;
    private void Start()
    {
        if (PlayerPrefs.GetInt("isBuy1") == 1)
        {
            Destroy(gameObject);
        }
    }
    private void FixedUpdate()
    {
        if ((transform.position.x > -1.46f && transform.position.y < 1.05f))
        {
            transform.position = new Vector2(transform.position.x - 0.085f, transform.position.y + 0.055f);
            transform.localScale = new Vector3(transform.localScale.x + 0.02f, transform.localScale.y + 0.02f, transform.localScale.z);
        }
        pl1.SetActive(false);
    }
    private void Update()
    {
        if (PlayerPrefs.GetInt("isBuy1") == 1)
        {
            Destroy(gameObject);
        }
    }
    private void OnMouseUp()
    {
        if (PlayerPrefs.GetInt("CCoin.s") > 99)
        {
            pl1.SetActive(true);
            PlayerPrefs.SetInt("CCoin.s", PlayerPrefs.GetInt("CCoin.s") - 100);
            PlayerPrefs.SetInt("isBuy1", 1);
            Destroy(gameObject);
        }
    }
}
