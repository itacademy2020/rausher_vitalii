﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartWeed : MonoBehaviour
{
    public GameObject MainCamera;

    public GameObject restart;

    private bool isRestart;

    private void Update()
    {
        isRestart = false;
    }

    private void OnMouseEnter()
    {
        restart.SetActive(true);
    }

    private void OnMouseOver()
    {
        restart.SetActive(true);
        if (isRestart)
        {
            SceneManager.LoadScene("WeedSampleScene");
            MainCamera.transform.position = new Vector3(0, 0, -10f);
            isRestart = false;
        }
    }

    private void OnMouseExit()
    {
        restart.SetActive(false);
    }
    private void OnMouseUp()
    {
        isRestart = true;
    }
}