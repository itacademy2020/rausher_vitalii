﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gopnikstart : MonoBehaviour
{
    public static bool isGopnik = false;
    public GameObject gb;
    public GameObject txt;
    private bool isSetting;

    private void Update()
    {
        isSetting = false;
        if (isGopnik == false)
        {
            gb.SetActive(false);
        }
        if (isGopnik == true)
        {
            gb.SetActive(true);
        }
    }
    private void OnMouseEnter()
    {
        gb.SetActive(true);
    }
    private void OnMouseOver()
    {
        gb.SetActive(true);
        if (isSetting)
        {
            if (PlayerPrefs.GetInt("isBuyGop") != 1)
            {
                if (PlayerPrefs.GetInt("CCoin.s") > 499 )
                {
                    PlayerPrefs.SetInt("CCoin.s", CoinsCollect.collectC / 2 - 500);
                    PlayerPrefs.SetInt("isBuyGop", 1);
                }
                else
                {
                    txt.SetActive(true);
                }
            }
            if (PlayerPrefs.GetInt("isBuyGop") == 1)
            {
                isGopnik = !isGopnik;
            }
            isSetting = false;
        }
    }
    private void OnMouseExit()
    {
        if (isGopnik == false) 
        {       
            gb.SetActive(false);
            txt.SetActive(false);
        }
    }
    private void OnMouseUp()
    {
        isSetting = true;
    }
}
