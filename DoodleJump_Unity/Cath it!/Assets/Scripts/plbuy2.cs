﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class plbuy2 : MonoBehaviour
{
    public GameObject pl2;
    private void Start()
    {
        if (PlayerPrefs.GetInt("isBuy2") == 1)
        {
            Destroy(gameObject);
        }
    }
    private void FixedUpdate()
    {
        if ((transform.position.y < 1.67f))
        {
            transform.position = new Vector2(transform.position.x, transform.position.y + 0.1f);
            transform.localScale = new Vector3(transform.localScale.x + 0.02f, transform.localScale.y + 0.02f, transform.localScale.z);
        }
        pl2.SetActive(false);
    }
    private void Update()
    {
        if (PlayerPrefs.GetInt("isBuy2") == 1)
        {
            Destroy(gameObject);
        }
    }
    private void OnMouseUp()
    {
        if (PlayerPrefs.GetInt("CCoin.s") > 299)
        {
            pl2.SetActive(true);
            PlayerPrefs.SetInt("CCoin.s", PlayerPrefs.GetInt("CCoin.s") - 300);
            PlayerPrefs.SetInt("isBuy2", 1);
            Destroy(gameObject);
        }
    }
}
