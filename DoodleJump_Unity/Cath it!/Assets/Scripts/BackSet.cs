﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackSet : MonoBehaviour
{
    public GameObject back;
    public static bool set;
    public static bool isBack = false;
    private bool isBackX;

    private void Awake()
    {
        set = true;
    }
    private void Update()
    {
        isBackX = false;
    }
    private void OnMouseEnter()
    {
        back.SetActive(true);
    }
    private void OnMouseOver()
    {
        back.SetActive(true);
        if (isBackX)
        {
            SceneManager.LoadScene("main");
            isBackX = false;
            isBack = true;
        }
    }
    private void OnMouseExit()
    {
        back.SetActive(false);
    }
    private void OnMouseUp()
    {
        isBackX = true;
        isBack = false;
    }
}