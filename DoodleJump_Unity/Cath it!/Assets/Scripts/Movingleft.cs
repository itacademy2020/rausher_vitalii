﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movingleft : MonoBehaviour
{
    private void FixedUpdate()
    {
        if ((transform.position.x < 1.0f && transform.position.y < -0.5f))
        {
            transform.position = new Vector2(transform.position.x + 0.1f, transform.position.y + 0.2f);
            transform.localScale = new Vector3(transform.localScale.x - 0.1f, transform.localScale.y - 0.1f, transform.localScale.z);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
