﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pl2 : MonoBehaviour
{
    public GameObject p2;
    private bool click;
    private void FixedUpdate()
    {
        if ((transform.position.y < 1.67f))
        {
            transform.position = new Vector2(transform.position.x, transform.position.y + 0.1f);
            transform.localScale = new Vector3(transform.localScale.x + 0.015f, transform.localScale.y + 0.015f, transform.localScale.z);
        }
        if (PlatformSpawn.xxx == 2)
        {
            p2.SetActive(true);
        }
        else
        {
            p2.SetActive(false);
        }
    }
    private void OnMouseOver()
    {
        p2.SetActive(true);
        if (click)
        {
            PlatformSpawn.xxx = 2;
            click = false;
        }
    }
    private void OnMouseUp()
    {
        click = true;
    }
}