﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;


public class PlatformSpawnWeedMode : MonoBehaviour
{
    public GameObject platform;

    public GameObject player;
    private float x, xx;

    AudioSource audioSource;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        StartCoroutine(Spawn());

        if (!Sound.isSoundOn)
            audioSource.mute = !audioSource.mute;
    }
    private void FixedUpdate()
    {
        transform.Translate(0, 0.01f, 0);
    }

    IEnumerator Spawn()
    {
        while (true)
        {
            if (transform.position.y + 1.6f < player.transform.position.y)
            {
                Instantiate(platform, new Vector2(Random.Range(-2f, 2f), transform.position.y + 6f), Quaternion.identity);
            }
            if (transform.position.y - 0.7f < player.transform.position.y)
            {
                Instantiate(platform, new Vector2(Random.Range(-2f, 2f), transform.position.y + 9.5f), Quaternion.identity);
                yield return new WaitForSeconds(0.3f);
            }
            else
            {
                Instantiate(platform, new Vector2(Random.Range(-2f, 2f), transform.position.y + 11f), Quaternion.identity);
                yield return new WaitForSeconds(0.3f);
            }
        }
    }
}