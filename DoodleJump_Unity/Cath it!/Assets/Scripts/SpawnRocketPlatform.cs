﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnRocketPlatform : MonoBehaviour
{
    public GameObject coins;
    public GameObject pltf;
    public GameObject pltfgop;
    public GameObject platform;
    public GameObject platform1;
    public GameObject platform2;
    public GameObject platform3;
    public GameObject platform4;
    public GameObject platform5;
    public GameObject platform6;
    public GameObject platform7;
    private float x;

    private void Awake()
    {
        if (PlatformSpawn.xxx == 0)
        {
            platform = pltf;
        }
        if (PlatformSpawn.xxx == 1)
        {
            platform = platform1;
        }
        if (PlatformSpawn.xxx == 2)
        {
            platform = platform2;
        }
        if (PlatformSpawn.xxx == 3)
        {
            platform = platform3;
        }
        if (PlatformSpawn.xxx == 4)
        {
            platform = platform4;
        }
        if (PlatformSpawn.xxx == 5)
        {
            platform = platform5;
        }
        if (PlatformSpawn.xxx == 6)
        {
            platform = platform6;
        }
        if (gopnikstart.isGopnik == true)
        {
            platform = pltfgop;
        }
    }

    void Start()
    {
        StartCoroutine(Spawn());
    }
    IEnumerator Spawn()
    {
        while (true)
        {
            x = Random.Range(-2.2f, 2.2f);
            {
                Instantiate(platform, new Vector2(x, transform.position.y + 11f), Quaternion.identity);
                Instantiate(coins, new Vector2(x, transform.position.y + 11.3f), Quaternion.identity);
                yield return new WaitForSeconds(10f);
            }
        }
    }
}
