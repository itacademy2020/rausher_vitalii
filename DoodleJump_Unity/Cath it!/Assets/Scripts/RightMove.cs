﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightMove : MonoBehaviour
{
    public Transform player;
    public Transform pl;
    public Transform plhell;
    public Transform plgop;
    [SerializeField]
    private float speed = 10f;

    private void Awake()
    {
        player = pl;
        if (PlatformSpawn.xxx == 2)
        {
            player = plhell;
        }
        if (gopnikstart.isGopnik == true)
        {
            player = plgop;
        }
    }
    private void OnMouseDown()
    {
        if (gopnikstart.isGopnik == false && PlatformSpawn.xxx != 2) 
        { 
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            player.position = new Vector2(player.position.x + speed * Time.deltaTime, player.position.y);
            player.transform.localScale = new Vector3(1.69924f, player.localScale.y, player.localScale.z);

            if (player.position.x > 3.5f)
            {
                player.position = new Vector2(-3.5f, player.position.y);
            }
        }

        if (PlatformSpawn.xxx == 2 && gopnikstart.isGopnik == false)
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            player.position = new Vector2(player.position.x + speed * Time.deltaTime, player.position.y);
            player.transform.localScale = new Vector3(0.63f, player.localScale.y, player.localScale.z);

            if (player.position.x > 3.5f)
            {
                player.position = new Vector2(-3.5f, player.position.y);
            }
        }
        if (gopnikstart.isGopnik == true)
            {
                Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                player.position = new Vector2(player.position.x + speed * Time.deltaTime, player.position.y);
                player.transform.localScale = new Vector3(0.2475516f, player.localScale.y, player.localScale.z);

                if (player.position.x > 3.5f)
                {
                    player.position = new Vector2(-3.5f, player.position.y);
                }
            }
    }
    private void OnMouseDrag()
    {
        if (gopnikstart.isGopnik == false && PlatformSpawn.xxx != 2)
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            player.position = new Vector2(player.position.x + speed * Time.deltaTime, player.position.y);
            player.transform.localScale = new Vector3(1.69924f, player.localScale.y, player.localScale.z);

            if (player.position.x > 3.5f)
            {
                player.position = new Vector2(-3.5f, player.position.y);
            }
        }
            else
        if (gopnikstart.isGopnik == true)
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            player.position = new Vector2(player.position.x + speed * Time.deltaTime, player.position.y);
            player.transform.localScale = new Vector3(0.2475516f, player.localScale.y, player.localScale.z);

            if (player.position.x > 3.5f)
            {
                player.position = new Vector2(-3.5f, player.position.y);
            }
        }
        if (PlatformSpawn.xxx == 2 && gopnikstart.isGopnik == false)
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            player.position = new Vector2(player.position.x + speed * Time.deltaTime, player.position.y);
            player.transform.localScale = new Vector3(0.63f, player.localScale.y, player.localScale.z);

            if (player.position.x > 3.5f)
            {
                player.position = new Vector2(-3.5f, player.position.y);
            }
        }
    }
}