﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ExitGame : MonoBehaviour
{
    private void OnMouseDown()
    {
        PlayerPrefs.SetInt("CCoin.s", CoinsCollect.collectC / 2);
        Application.Quit();
    }
}
