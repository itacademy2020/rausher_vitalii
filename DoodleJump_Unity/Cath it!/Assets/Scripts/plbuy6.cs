﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class plbuy6 : MonoBehaviour
{
    public GameObject pl6;
    private void Start()
    {
        if (PlayerPrefs.GetInt("isBuy6") == 1)
        {
            Destroy(gameObject);
        }
    }
    private void FixedUpdate()
    {
        if ((transform.position.x > -1f && transform.position.y > -1.85f))
        {
            transform.position = new Vector2(transform.position.x - 0.05f, transform.position.y - 0.093f);
            transform.localScale = new Vector3(transform.localScale.x + 0.015f, transform.localScale.y + 0.015f, transform.localScale.z);
        }
        pl6.SetActive(false);
    }
    private void Update()
    {
        if (PlayerPrefs.GetInt("isBuy6") == 1)
        {
            Destroy(gameObject);
        }
    }
    private void OnMouseUp()
    {
        if (PlayerPrefs.GetInt("CCoin.s") > 499)
        {
            pl6.SetActive(true);
            PlayerPrefs.SetInt("CCoin.s", PlayerPrefs.GetInt("CCoin.s") - 500);
            PlayerPrefs.SetInt("isBuy6", 1);
            Destroy(gameObject);
        }
    }
}
