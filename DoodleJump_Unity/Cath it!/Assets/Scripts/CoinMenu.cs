﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CoinMenu : MonoBehaviour
{
    public GameObject coin;
    public GameObject back;
    public GameObject set;
    public GameObject txt;
    public bool isWeed;
    private bool click;

    private void Update()
    {
        click = false;
    }
    private void OnMouseEnter()
    {
        set.SetActive(true);
        coin.SetActive(true);
    }
    private void OnMouseOver()
    {
        set.SetActive(true);
        coin.SetActive(true);
        if (click)
        {

            if (PlayerPrefs.GetInt("isBuyWeed") != 1) {
                if (PlayerPrefs.GetInt("CCoin.s") > 420)
                {
                    CoinsCollect.collectC -= 840;
                    PlayerPrefs.SetInt("CCoin.s", CoinsCollect.collectC / 2);
                    PlayerPrefs.SetInt("isBuyWeed", 1);
                }
                else
                {
                    txt.SetActive(true);
                }
            }
            if (PlayerPrefs.GetInt("isBuyWeed") == 1)
            {
                SceneManager.LoadScene("WeedSampleScene");
            }
            click = false;
        }
    }
    private void OnMouseExit()
    {
        set.SetActive(false);
        coin.SetActive(false);
        txt.SetActive(false);
    }
    private void OnMouseUp()
    {
        click = true;
    }
}
