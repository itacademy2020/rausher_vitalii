﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundOn : MonoBehaviour
{
    public GameObject sounOn;

    private void FixedUpdate()
    {
        if (Sound.isSoundOn)
        {
            sounOn.SetActive(true);
        }
        else
        {
            sounOn.SetActive(false);
        }
    }
    private void OnMouseEnter()
    {
        if (Sound.isSoundOn)
        {
            sounOn.SetActive(true);
        }
        else
        {
            sounOn.SetActive(false);
        }
    }
    private void OnMouseOver()
    {
        if (Sound.isSoundOn)
        {
            sounOn.SetActive(true);
        }
        else
        {
            sounOn.SetActive(false);
        }
    }
    private void OnMouseExit()
    {
        if (Sound.isSoundOn)
        {
            sounOn.SetActive(true);
        }
        else
        {
            sounOn.SetActive(false);
        }
    }
    private void OnMouseUp()
    {
        Sound.isSoundOn = true;
    }
}
