﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiePlatform : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Die")
        {
            Destroy(gameObject);
        }
    }
}
