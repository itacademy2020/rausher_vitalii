﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SelecrRight : MonoBehaviour
{
    public GameObject select;
    public static bool isRightMove;
    public static int pl;
    private bool isSelect;
    private void Update()
    {
        isSelect = false;
        isRightMove = false;
    }
    private void OnMouseEnter()
    {
        select.SetActive(true);
    }
    private void OnMouseOver()
    {
        select.SetActive(true);
        if (isSelect)
        {
            pl++;
            if (pl == 7)
            {
                pl = 0;
            }
        }
    }
    private void OnMouseExit()
    {
        select.SetActive(false);
    }
    private void OnMouseUp()
    {
        isSelect = true;
    }
}
