﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class Player : MonoBehaviour
{
    public static bool lose = false;

    public GameObject MainCamera;

    public GameObject restart;

    public GameObject exit;

    public GameObject exit2;

    public Text text;
    public Text textscore;
    int xx;

    public float horizontalSpeed = 0.1f;
    float speedX;
    int score = 0;
    public float verticalImpulse = 5f;
    Rigidbody2D rb;
    bool isGrounded;
    bool isBatut;
    bool isRocket;
    public float JumpForce = 11f;

    private void Awake()
    {
        lose = false;
        score = 0;
    }

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {

        if (transform.position.x > 1.39f && transform.position.y == 0.5f)
        {
            rb.velocity = Vector2.left * JumpForce;
        }
        if (lose)
        {
            MainCamera.transform.Translate(0, -0.5f, 0);
            restart.SetActive(true);
            exit.SetActive(true);
            exit2.SetActive(true);

        }
        if (transform.position.x == 3.5)
        {
            transform.position = new Vector2(-3.5f, transform.position.y);
        }
        if (isGrounded)
        {
            if (transform.position.y < MainCamera.transform.position.y - 1.5f)
            {
                rb.velocity = Vector2.up * (JumpForce + 2f);
            }
            else
            {
                rb.velocity = Vector2.up * JumpForce;
            }
            if (isBatut)
            {
                rb.velocity = Vector2.up * (JumpForce + 12f);
                isBatut = false;
            }
            if (isRocket)
            {
                transform.position = new Vector2(transform.position.x, MainCamera.transform.position.y + 4.3f);
                score += 1000;
                    isRocket = false;
            }
        }
        if (MainCamera.transform.position.y - 1.5f < transform.position.y)
        {
            if (MainCamera.transform.position.y + 4.2f < transform.position.y)
            {
                MainCamera.transform.Translate(0, 0.2f, 0);
            }
            else
            {
                MainCamera.transform.Translate(0, 0.07f, 0);
            }
        }
        transform.Translate(speedX, 0, 0);
        speedX = 0;
        if (PlayerPrefs.GetInt("Score") < score)
        {
            PlayerPrefs.SetInt("Score", score);
        }
        text.text = score + "";
        textscore.text = "High Score:\n" + PlayerPrefs.GetInt("Score") + "\n Your Score:   " + score;
    }

    private void Update()
    {
        if (transform.position.y > MainCamera.transform.position.y - 1.5f)
        {
            score++;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Platform")
        {
            isGrounded = true;
        }
        if (collision.gameObject.tag == "Die")
        {
            lose = true;
        }
        if (collision.gameObject.tag == "Batut")
        {
            isBatut = true;
        }
        if (collision.gameObject.tag == "Rocket")
        {
            isRocket = true;
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Platform")
        {
            isGrounded = false;
        }

    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Platform")
        {
            isGrounded = false;
        }

    }
}
