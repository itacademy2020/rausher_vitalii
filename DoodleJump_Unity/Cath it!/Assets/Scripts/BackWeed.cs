﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackWeed : MonoBehaviour
{
    public GameObject backWeedMode;

    private bool isBackWeedMode;

    private void Update()
    {
        isBackWeedMode = false;
    }

    private void OnMouseEnter()
    {
        backWeedMode.SetActive(true);
    }

    private void OnMouseOver()
    {
        backWeedMode.SetActive(true);
        if (isBackWeedMode)
        {
            SceneManager.LoadScene("WeedMode");
            isBackWeedMode = false;

        }
    }

    private void OnMouseExit()
    {
        backWeedMode.SetActive(false);
    }
    void OnMouseUp()
    {
        isBackWeedMode = true;
    }
}
