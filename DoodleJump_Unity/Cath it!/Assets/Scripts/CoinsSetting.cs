﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinsSetting : MonoBehaviour
{
    public Text txt;
    private void Update()
    {
        txt.text = PlayerPrefs.GetInt("CCoin.s").ToString();
    }
}
