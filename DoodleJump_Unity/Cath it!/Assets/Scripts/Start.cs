﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Start : MonoBehaviour
{
    public GameObject start;

    private bool isStart;

    private void Update()
    {
        isStart = false;
    }
    private void OnMouseEnter()
    {
        start.SetActive(true);
    }
    private void OnMouseOver()
    {
        start.SetActive(true);
        if (isStart)
        {
            if (SelectPlatformMenu.canStart == true)
            {
                SceneManager.LoadScene("SampleScene");
            }
            isStart = false;
        }
    }
    private void OnMouseExit()
    {
        start.SetActive(false);
    }
    private void OnMouseUp()
    {
        isStart = true;
    }
}