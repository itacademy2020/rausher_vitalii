﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class plbuy5 : MonoBehaviour
{
    public GameObject pl5;
    private void Start()
    {
        if (PlayerPrefs.GetInt("isBuy5") == 1)
        {
            Destroy(gameObject);
        }
    }
    private void FixedUpdate()
    {
        if ((transform.position.x < 1.17f && transform.position.y > -1.81f))
        {
            transform.position = new Vector2(transform.position.x + 0.05f, transform.position.y - 0.08f);
            transform.localScale = new Vector3(transform.localScale.x + 0.015f, transform.localScale.y + 0.015f, transform.localScale.z);
        }
        pl5.SetActive(false);
    }
    private void Update()
    {
        if (PlayerPrefs.GetInt("isBuy5") == 1)
        {
            Destroy(gameObject);
        }
    }
    private void OnMouseUp()
    {
        if (PlayerPrefs.GetInt("CCoin.s") > 399)
        {
            pl5.SetActive(true);
            PlayerPrefs.SetInt("CCoin.s", PlayerPrefs.GetInt("CCoin.s") - 400);
            PlayerPrefs.SetInt("isBuy5", 1);
            Destroy(gameObject);
        }
    }
}
