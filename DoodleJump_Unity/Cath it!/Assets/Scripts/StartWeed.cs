﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class StartWeed : MonoBehaviour
{
    public GameObject quitWeedMode;

    private bool isExitWeedMode;
    private void Update()
    {
        isExitWeedMode = false;
    }
    private void OnMouseEnter()
    {
        quitWeedMode.SetActive(true);
    }
    private void OnMouseOver()
    {
        quitWeedMode.SetActive(true);
        if (isExitWeedMode)
        {
            SceneManager.LoadScene("WeedSampleScene");
            isExitWeedMode = false;
        }
    }
    private void OnMouseExit()
    {
        quitWeedMode.SetActive(false);
    }
    private void OnMouseUp()
    {
        isExitWeedMode = true;
    }
}
