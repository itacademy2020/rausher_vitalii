﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static bool isJump;
    private bool isKiker;
    private bool isRail;
    [SerializeField] private bl_Joystick Joystick;

    [SerializeField] private float Speed = 0.4f;
    private int degress;
    private int kik;
    private void Start()
    {
        degress = 0;
        isKiker = false;
        isRail = false;
    }
    void Update()
    {
        float v = Joystick.Vertical;
        float h = Joystick.Horizontal;

        if (rotationright.rotate)
        {
            transform.eulerAngles = new Vector3(0, 0, degress);
            if (degress > -370)
            {
                degress -= 10;
            }
            if (degress < -361)
            {
                degress = 0;
                rotationright.rotate = false;
            }
        }
        if (rotationleft.rotate)
        {
            transform.eulerAngles = new Vector3(0, 0, degress);
            if (degress < 361)
            {
                degress += 10;
            }
            if (degress > 361)
            {
                degress = 0;
                rotationleft.rotate = false;
            }
        }
        if (isKiker)
        {
            transform.eulerAngles = new Vector3(0, 0, kik);
            if (kik < 30)
            {
                kik += 1;
            }
        }
        else if (isRail)
        {
            transform.eulerAngles = new Vector3(0, 0, kik);
            if (kik > -31)
            {
                kik -= 5;
            }
        }

        if (isKiker == false && isRail == false)
        {
            kik = 0;
            transform.eulerAngles = new Vector3(0, 0, degress);
        }

        Vector3 translate = (new Vector3(h, v, kik) * Time.deltaTime) * Speed;
        transform.Translate(translate);
        transform.position = new Vector3(transform.position.x, transform.position.y, 0f);
    }

   
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "ground")
        {
            isJump = true;
        }
        if (collision.gameObject.tag == "kiker")
        {
            isKiker = true;
        }
        if (collision.gameObject.tag == "handrail")
        {
            isRail = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "ground")
        {
            isJump = false;
        }
        if (collision.gameObject.tag == "kiker")
        {
            isKiker = false;
        }
        if (collision.gameObject.tag == "handrail")
        {
            isRail = false;
        }
    }

}
