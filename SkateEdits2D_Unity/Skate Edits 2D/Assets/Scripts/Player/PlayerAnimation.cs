﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{
    private Animator anim;
    Rigidbody2D rb;
    float x = 0;
    public float JumpForce;
    bool active;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }
    void Update()
    {
        if (isJump.isClick && PlayerController.isJump)
        {
            anim.SetTrigger("isOllie");

            rb.velocity = Vector3.up * JumpForce;

            x = 1;
        }
        if (x != 0)
        {
            transform.position = new Vector3(transform.position.x + 0.01f, transform.position.y, 5f);
            x++;
            if (x == 130) { x = 0; }
        }
    }


}
