﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuMoving : MonoBehaviour
{
    public GameObject play;
    public GameObject progress;
    public GameObject custom;
    public GameObject settings;
    public GameObject exit;
    void Awake()
    {
        play.transform.position = new Vector3(16f, play.transform.position.y, play.transform.position.z);
        progress.transform.position = new Vector3(16f, progress.transform.position.y, progress.transform.position.z);
        custom.transform.position = new Vector3(16f, custom.transform.position.y, custom.transform.position.z);
        settings.transform.position = new Vector3(16f, settings.transform.position.y, settings.transform.position.z);
        exit.transform.position = new Vector3(16f, exit.transform.position.y, exit.transform.position.z);
    }

    void Update()
    {
        if (play.transform.position.x > 5.453f)
        {
            play.transform.position = new Vector3(play.transform.position.x - 0.4f, play.transform.position.y, play.transform.position.z);
        }
        if (play.transform.position.x < 8f && progress.transform.position.x > 5.453f)
        {
            progress.transform.position = new Vector3(progress.transform.position.x - 0.4f, progress.transform.position.y, progress.transform.position.z);
        }
        if (progress.transform.position.x < 8f && custom.transform.position.x > 5.453f)
        {
            custom.transform.position = new Vector3(custom.transform.position.x - 0.4f, custom.transform.position.y, custom.transform.position.z);
        }
        if (custom.transform.position.x < 8f && settings.transform.position.x > 5.453f)
        {
            settings.transform.position = new Vector3(settings.transform.position.x - 0.4f, settings.transform.position.y, settings.transform.position.z);
        }
        if (settings.transform.position.x < 8f && exit.transform.position.x > 5.453f)
        {
            exit.transform.position = new Vector3(exit.transform.position.x - 0.4f, exit.transform.position.y, exit.transform.position.z);
        }
    }
}
