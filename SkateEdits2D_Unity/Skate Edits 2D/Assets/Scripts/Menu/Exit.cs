﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Exit : MonoBehaviour
{
    private bool isClick = false;
    private bool isSelect = false;

    void Update()
    {
        if (isClick)
        {
            transform.position = new Vector3(4.4f, transform.position.y, transform.position.z);
        }
        if (isClick == false && transform.position.x < 5.7f)
        {
            transform.position = new Vector3(5.2f, transform.position.y, transform.position.z);
        }
        if (isSelect)
        {
            Application.Quit();
            isSelect = false;
        }
    }
    private void OnMouseOver()
    {
        isClick = true;
    }
    private void OnMouseExit()
    {
        isClick = false;
    }
    private void OnMouseUp()
    {
        if (isClick)
        {
            isSelect = true;
        }
    }
}