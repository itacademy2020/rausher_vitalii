﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Back : MonoBehaviour
{
    private bool isClick = false;
    private bool isSelect = false;

    void Update()
    {
        if (isSelect)
        {
            SceneManager.LoadScene("MainMenu");
            Time.timeScale = 1;
            isSelect = false;
        }
    }
    private void OnMouseOver()
    {
        isClick = true;
    }
    private void OnMouseExit()
    {
        isClick = false;
    }
    private void OnMouseUp()
    {
        if (isClick)
        {
            isSelect = true;
        }
    }
}
