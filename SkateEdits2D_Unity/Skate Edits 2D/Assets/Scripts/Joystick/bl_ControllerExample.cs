﻿using UnityEngine;

public class bl_ControllerExample : MonoBehaviour {

	[SerializeField]private bl_Joystick Joystick;

    [SerializeField]private float Speed = 0.5f;

    void Update()
    {
        float v = Joystick.Vertical; 
        float h = Joystick.Horizontal;
        Vector3 translate = (new Vector3(h, v, 0) * Time.deltaTime) * Speed;
        transform.Translate(translate);
    }
}