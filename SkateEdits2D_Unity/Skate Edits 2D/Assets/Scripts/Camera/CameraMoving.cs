﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMoving : MonoBehaviour
{
    private void Start()
    {
        Time.timeScale = 1;
    }
    private void Update()
    {
            transform.position = new Vector3(transform.position.x + 2.5f * Time.deltaTime, transform.position.y, transform.position.z);
    }
}
