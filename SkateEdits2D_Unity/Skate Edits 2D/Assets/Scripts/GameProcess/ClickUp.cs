﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickUp : MonoBehaviour
{
    public GameObject up;
    private bool isClick;

    private void Update()
    {
        isClick = false;
    }
    private void OnMouseDown()
    {
        up.SetActive(true);
    }
    private void OnMouseUp()
    {
        up.SetActive(false);
    }
    private void OnMouseExit()
    {
        up.SetActive(false);
    }
}
