﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{
    public static bool isPaused = false;
    public GameObject back;
    public GameObject exit;
    private void Start()
    {
        isPaused = false;
    }
    private void Update()
    {
        if (isPaused)
        {
            exit.SetActive(true);
            back.SetActive(true);
            Time.timeScale = 0;
        }
        else
        {
            exit.SetActive(false);
            back.SetActive(false);
            Time.timeScale = 1;
        }
    }
    private void OnMouseDown()
    {
        isPaused = !isPaused;
    }
}
