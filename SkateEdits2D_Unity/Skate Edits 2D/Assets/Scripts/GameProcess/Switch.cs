﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switch : MonoBehaviour
{
    private Animator anim;
    private int degress = 0;
    bool isClick;
    bool goofy = false;

    private void Start()
    {
        anim = GetComponent<Animator>();
    }
    void FixedUpdate()
    {
        anim.SetBool("Goffy", goofy);
        if (isClick)
        {
            transform.eulerAngles = new Vector3(0, degress, 0);
            if (degress < 360)
            {
                degress += 10;
                if (degress == 50)
                {
                    goofy = !goofy;
                }
            }
            if (degress == 360)
            {
                degress = 0;
                isClick = false;
            }
        }
    }
    private void OnMouseDown()
    {
        isClick = true;
    }
}
