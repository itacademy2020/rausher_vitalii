﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoving : MonoBehaviour
{
    public GameObject player;

    [SerializeField] private bl_Joystick Joystick;

    [SerializeField] private float Speed = 0.4f;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float h = Joystick.Horizontal;
        if (h > 0)
        {
            Vector3 translate = (new Vector3(h, 0, 0) * Time.deltaTime) * Speed;
            transform.Translate(translate);
        }

        if (player.transform.position.x > transform.position.x)
        {
            transform.position = new Vector3(transform.position.x + 0.1f, transform.position.y, transform.position.z);
            player.transform.position = new Vector3(player.transform.position.x - 0.05f, player.transform.position.y, 5f);
        }
        if (player.transform.position.x > transform.position.x - 2f && player.transform.position.x < transform.position.x )
        {
            transform.position = new Vector3(transform.position.x + 0.05f, transform.position.y, transform.position.z);
            player.transform.position = new Vector3(player.transform.position.x - 0.02f, player.transform.position.y, 5f);
        }
        if (player.transform.position.x > transform.position.x - 3f && player.transform.position.x < transform.position.x - 2f)
        {
            transform.position = new Vector3(transform.position.x + 0.03f, transform.position.y, transform.position.z);
            player.transform.position = new Vector3(player.transform.position.x - 0.01f, player.transform.position.y, 5f);
        }
        if (player.transform.position.x > transform.position.x - 4f && player.transform.position.x < transform.position.x - 3f)
        {
            transform.position = new Vector3(transform.position.x + 0.01f, transform.position.y, transform.position.z);
            player.transform.position = new Vector3(player.transform.position.x - 0.005f, player.transform.position.y, 5f);
        }
    }
}
