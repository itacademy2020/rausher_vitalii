﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class kiker : MonoBehaviour
{
    public Rigidbody2D player;
    bool active = false;


    // Update is called once per frame
    void Update()
    {
        if (active)
        {
            player.velocity = Vector2.right * 20f;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "kiker")
        {
            active = true;
        }
        else
        {
            active = false;
        }
    }
}
