﻿uses
  graphABC, Utils;


type
  
  point = record
    x, y: integer;
  end;


const
  pl = 14;
  sl = 60;


var
  
  doodler, doodlerok4, doodlerok5, ok, unok, bo4ka, background, raceta, coin, platform, pryjina, platformset, doodlerok3, snow, backgroundset, settingbackground, poloska1, poloska2, monster1, monster2, monster3, poloska3, poloska4, poloska5, poloska6, poloska7, restart, doodlerok1, backgroundfm1, backgroundfm2, backgroundfm3, backgroundfm4, backgroundfm5, backgroundfm6, backgroundfm7, doodlerok2, doodlerf1, doodlerf2, doodlerf3, doodlerf4, doodlerf5, doodlerf6, doodlerf7, doodlerf8, doodlerf9, doodlerf10, doodlerf11, doodlerf12, platformf1, platformf2, platformf3, platformf4, platformf5, platformf6, platformf7, platformf8: picture;
  Left, Right, x, fps, y, i, h, score, sc, cs2, sc1, cs, sc2, sc3, sc4, sc5, sc6, sc7, err, err1, err2, err3, err4, err5, err6, err7, x1, y3, x4, y4, valik: integer;
  vx, vy, vy1, vym, sym: real;
  platforms: array [1..20] of point;
  snows: array [1..100] of point;
  game, active, how, settings, racketa, racketka, coins, attack, bochka, magaz, magazz, war, superhard, gameover, fpc, sound, prujina, back, records, snowing, readbackground, readdoodler, readname, sec, monstr, monstr1, monstr2, monstr3, start, monstrleft, monstrright, rb1, rb2, rb3, rb4, rb5, rb6, rb7, rd1, rd2, rd3, rd4, rd5, rd6, rd7, rd8, rd9, rd10, rd11, rd12, readplatform, rp1, rp2, rp3, rp4, rp5, rp6, rp7, rp8: boolean;
  t: text;
  tr: text;
  kt: text;
  DB: array[1..10] of string;
  DBR: array[1..15] of string;
  KAT: array[1..30] of string;


procedure KeyDown(Key: integer);
begin
  if Key = vk_Left then Left := 1;
  if Key = vk_Right then Right := 1;
  if key = vk_Escape then begin; magazz := false;magaz := false; end;
  if key = vk_Escape then begin; active := true;game := false; end;
  if key = vk_Escape then begin; active := true;readdoodler := false; end;
  if key = vk_Escape then begin; active := true;records := false; end;
  if key = vk_Escape then begin; active := true;readplatform := false; end;
  if key = vk_Escape then begin; active := true;readbackground := false; end;
  if key = vk_Escape then begin; active := true;how := false; end;
  if (key = vk_Escape) and (settings = true) then begin; active := true;settings := false;vym := 0; end;
  if key = vk_Escape then begin; active := true;gameover := false; end;
  if (Key = vk_R) and (gameover = true) then begin; game := true;gameover := false; vy := 0; end;
  if (Key = vk_Q) and (active = true) then begin; records := true; end;
  if (Key = vk_Enter) and (gameover = true) then begin; game := true;gameover := false; vy := 0; end;
  if (Key = vk_Escape) and (gameover = true) then begin; gameover := false; end;
  if (Key = vk_Escape) and (game = true) then begin; game := false; end;
  if (Key = vk_F) and (active = true) or (Key = vk_Enter) and (active = true) then begin; game := true end;
  if (Key = vk_Tab) and (active = true) then begin; settings := true end;
  if Key = vk_f4 then halt;
  if ((Key = vk_Space) and (game = true)) or ((Key = vk_Up) and (game = true)) then attack := true;
end;



procedure KeyUp(Key: integer);
begin
  if Key = vk_Left then Left := 0;
  if Key = vk_Right then Right := 0;
end;



procedure MouseUp(x, y, mb: integer);
begin
  if (mb = 1) and (x > 210) and (x < 314) and (y > 426) and (y < 444) and (active = true) then begin; MinimizeWindow; Execute('https://www.instagram.com/el_zero_0/'); end;
  if (mb = 1) and (x > 0) and (x < 110) and (y > 426) and (y < 444) and (active = true) then begin; MinimizeWindow; Execute('https://www.instagram.com/explore/locations/895342990628134/aroma-kava-if/'); end;
  if (mb = 1) and (x > 80) and (x < 234) and (y > 50) and (y < 90) and (active = true) then begin; game := true;active := false; end;
  if (mb = 1) and (x > 64) and (x < 252) and (y > 238) and (y < 347) and (active = true) then begin; records := true;active := false; end;
  if (mb = 1) and (x > 80) and (x < 234) and (y > 97) and (y < 137) and (active = true) then begin; how := true;active := false; end;
  if (mb = 1) and (x > 80) and (x < 234) and (y > 144) and (y < 184) and (active = true) then begin; settings := true;active := false; end;
  if (mb = 1) and (x > 35) and (x < (35 + 57)) and (y > 15) and (y < 95) and (magazz = true) then bochka := true;
  if (mb = 1) and (x > 80) and (x < 234) and (y > 191) and (y < 231) and (active = true) then active := false;
  if (mb = 1) and (x > 100) and (x < 300) and (y > 133) and (y < 163) and (gameover = true) then begin game := true; gameover := false; end;
  if (mb = 1) and (x > 148) and (x < 173) and (y > 152) and (y < 166) and (settings = true) then begin; fpc := true; end;
  if (mb = 1) and (x > 148) and (x < 173) and (y > 232) and (y < 246) and (settings = true) then begin; snowing := true; end;
  if (mb = 1) and (x > 174) and (x < 206) and (y > 232) and (y < 246) and (settings = true) then begin; snowing := false; end;
  if (mb = 1) and (x > 148) and (x < 173) and (y > 252) and (y < 266) and (settings = true) then begin; magaz := true; end;
  if (mb = 1) and (x > 174) and (x < 206) and (y > 252) and (y < 266) and (settings = true) then begin; magaz := false; end;
  if (mb = 1) and (x > 174) and (x < 206) and (y > 152) and (y < 166) and (settings = true) and (active = false) then begin; fpc := false; end;
  if (mb = 1) and (x > 148) and (x < 173) and (y > 212) and (y < 226) and (settings = true) then begin; superhard := true; end;
  if (mb = 1) and (x > 174) and (x < 206) and (y > 212) and (y < 226) and (settings = true) then begin; superhard := false; end;
  if (mb = 1) and (x > 148) and (x < 173) and (y > 112) and (y < 126) and (settings = true) then begin; sound := true; end;
  if (mb = 1) and (x > 148) and (x < 200) and (y > 50) and (y < 62) and (settings = true) then begin; readname := true; end;
  if (mb = 1) and (x > 174) and (x < 206) and (y > 112) and (y < 126) and (settings = true) then begin; sound := false; end;
  if (mb = 1) and (x > 122) and (x < (297 + 45)) and (y > 292) and (y < (297 + 45)) and (gameover = true) then begin; game := true;gameover := false; end;
  if (mb = 1) and (x > 4) and (x < 64) and (y > 410) and (y < 435) and (settings = true) then begin; active := true;settings := false; end;
  if (mb = 1) and (x > 4) and (x < 64) and (y > 410) and (y < 435) and (magazz = true) then begin; magaz := false;;magazz := false; end;
  if (mb = 1) and (x > 4) and (x < 64) and (y > 410) and (y < 435) and (readdoodler = true) then begin; active := true;readdoodler := false; end;
  if (mb = 1) and (x > 4) and (x < 64) and (y > 410) and (y < 435) and (readbackground = true) then begin; active := true;readbackground := false; end;
  if (mb = 1) and (x > 4) and (x < 64) and (y > 410) and (y < 435) and (readplatform = true) then begin; active := true;readplatform := false; end;
  if (mb = 1) and (x > 4) and (x < 64) and (y > 410) and (y < 435) and (how = true) then begin; active := true;how := false; end;
  if (mb = 1) and (x > 4) and (x < 64) and (y > 410) and (y < 435) and (records = true) then begin; active := true;records := false; end;
  if (mb = 1) and (x > 218) and (x < (220 + 59)) and (y > 42) and (y < (44 + 59)) and (settings = true) then begin; readdoodler := true;settings := false; end;
  if (mb = 1) and (x > 215) and (x < (217 + 59)) and (y > 191) and (y < (193 + 59)) and (settings = true) then begin; readplatform := true;settings := false; end;
  if (mb = 1) and (x > 215) and (x < (217 + 59)) and (y > 101) and (y < (103 + 87)) and (settings = true) then begin; readbackground := true;settings := false; end;
  if (mb = 1) and (x > 3) and (x < 63) and (y > 5) and (y < 62) and (readdoodler = true) then begin; rd1 := true; end;
  if (mb = 1) and (x > 63) and (x < 120) and (y > 5) and (y < 62) and (readdoodler = true) then begin; rd2 := true; end;
  if (mb = 1) and (x > 123) and (x < 180) and (y > 5) and (y < 62) and (readdoodler = true) then begin; rd3 := true; end;
  if (mb = 1) and (x > 186) and (x < 243) and (y > 5) and (y < 62) and (readdoodler = true) then begin; rd4 := true; end;
  if (mb = 1) and (x > 249) and (x < 306) and (y > 5) and (y < 62) and (readdoodler = true) then begin; rd5 := true; end;
  if (mb = 1) and (x > 3) and (x < 60) and (y > 86) and (y < 143) and (readdoodler = true) then begin; rd6 := true; end;
  if (mb = 1) and (x > 63) and (x < 120) and (y > 86) and (y < 143) and (readdoodler = true) then begin; rd7 := true; end;
  if (mb = 1) and (x > 123) and (x < 180) and (y > 86) and (y < 143) and (readdoodler = true) then begin; rd8 := true; end;
  if (mb = 1) and (x > 186) and (x < 243) and (y > 86) and (y < 143) and (readdoodler = true) then begin; rd9 := true; end;
  if (mb = 1) and (x > 3) and (x < 60) and (y > 162) and (y < 219) and (readdoodler = true) then begin; rd10 := true; end;
  if (mb = 1) and (x > 63) and (x < 120) and (y > 162) and (y < 219) and (readdoodler = true) then begin; rd11 := true; end;
  if (mb = 1) and (x > 123) and (x < 180) and (y > 162) and (y < 219) and (readdoodler = true) then begin; rd12 := true; end;
  if (mb = 1) and (x > 3) and (x < 60) and (y > 10) and (y < 25) and (readplatform = true) then begin; rp1 := true; end;
  if (mb = 1) and (x > 63) and (x < 120) and (y > 10) and (y < 25) and (readplatform = true) then begin; rp2 := true; end;
  if (mb = 1) and (x > 123) and (x < 180) and (y > 10) and (y < 25) and (readplatform = true) then begin; rp3 := true; end;
  if (mb = 1) and (x > 186) and (x < 243) and (y > 10) and (y < 25) and (readplatform = true) then begin; rp4 := true; end;
  if (mb = 1) and (x > 249) and (x < 306) and (y > 10) and (y < 25) and (readplatform = true) then begin; rp5 := true; end;
  if (mb = 1) and (x > 3) and (x < 60) and (y > 30) and (y < 45) and (readplatform = true) then begin; rp6 := true; end;
  if (mb = 1) and (x > 63) and (x < 120) and (y > 30) and (y < 45) and (readplatform = true) then begin; rp7 := true; end;
  if (mb = 1) and (x > 123) and (x < 180) and (y > 30) and (y < 45) and (readplatform = true) then begin; rp8 := true; end;  
  if (mb = 1) and (x > 3) and (x < 60) and (y > 10) and (y < 95) and (readbackground = true) then begin; rb1 := true; end;
  if (mb = 1) and (x > 63) and (x < 120) and (y > 10) and (y < 95) and (readbackground = true) then begin; rb2 := true; end;
  if (mb = 1) and (x > 123) and (x < 180) and (y > 10) and (y < 95) and (readbackground = true) then begin; rb3 := true; end;
  if (mb = 1) and (x > 186) and (x < 243) and (y > 10) and (y < 95) and (readbackground = true) then begin; rb4 := true; end;
  if (mb = 1) and (x > 249) and (x < 306) and (y > 10) and (y < 95) and (readbackground = true) then begin; rb5 := true; end;
  if (mb = 1) and (x > 3) and (x < 60) and (y > 115) and (y < 200) and (readbackground = true) then begin; rb6 := true; end;
  if (mb = 1) and (x > 63) and (x < 120) and (y > 115) and (y < 200) and (readbackground = true) then begin; rb7 := true; end;
  
end;



begin
  window.Caption := 'Zero';
  SetWindowSize(314, 444);
  window.IsFixedSize := true;
  LockDrawing;
  OnKeyDown := KeyDown;
  OnKeyUp := KeyUp;
  OnMouseUp := MouseUp;
  pryjina := Picture.Create('data\pryjina.png');
  raceta := Picture.Create('data\racketa.png');
  coin := Picture.Create('data\coin.png');
  ok := Picture.Create('data\ok.png');
  bo4ka := Picture.Create('data\bo4ka.png');
  unok := Picture.Create('data\unok.png');
  settingbackground := Picture.Create('data\settingbackground.png');
  backgroundset := Picture.Create('data\backgroundset1.png');
  platform := picture.Create('data\platformset1.png');
  snow := Picture.Create('data\snow.png');
  
  monster1 := Picture.Create('data\monster1.png');
  monster2 := Picture.Create('data\monster2.png');
  monster3 := Picture.Create('data\monster3.png');
  
  poloska1 := Picture.Create('data\poloska.png');
  poloska2 := Picture.Create('data\poloska.png');
  poloska3 := Picture.Create('data\poloska.png');
  poloska4 := Picture.Create('data\poloska.png');
  poloska5 := Picture.Create('data\poloska.png');
  poloska6 := Picture.Create('data\poloska.png');
  poloska7 := Picture.Create('data\poloska.png');
  
  doodlerf1 := Picture.Create('data\doodler1.png');
  doodlerf2 := Picture.Create('data\doodler2.png');
  doodlerf3 := Picture.Create('data\doodler3.png');
  doodlerf4 := Picture.Create('data\doodler4.png');
  doodlerf5 := Picture.Create('data\doodler5.png');
  doodlerf6 := Picture.Create('data\doodler6.png');
  doodlerf7 := Picture.Create('data\doodler7.png');
  doodlerf8 := Picture.Create('data\doodler8.png');
  doodlerf9 := Picture.Create('data\doodler9.png');
  doodlerf10 := Picture.Create('data\doodler10.png');
  doodlerf11 := Picture.Create('data\doodler11.png');
  doodlerf12 := Picture.Create('data\doodler12.png');
  
  platformf1 := Picture.Create('data\platformset1.png');
  platformf2 := Picture.Create('data\platformset2.png');
  platformf3 := Picture.Create('data\platformset3.png');
  platformf4 := Picture.Create('data\platformset4.png');
  platformf5 := Picture.Create('data\platformset5.png');
  platformf6 := Picture.Create('data\platformset6.png');
  platformf7 := Picture.Create('data\platformset7.png');
  platformf8 := Picture.Create('data\platformset8.png');
  
  backgroundfm1 := Picture.Create('data\backgroundset1.png');
  backgroundfm2 := Picture.Create('data\backgroundset2.png');
  backgroundfm3 := Picture.Create('data\backgroundset3.png');
  backgroundfm4 := Picture.Create('data\backgroundset4.png');
  backgroundfm5 := Picture.Create('data\backgroundset5.png');
  backgroundfm6 := Picture.Create('data\backgroundset6.png');
  backgroundfm7 := Picture.Create('data\backgroundset7.png');
  
  
  active := true;
  game := false;
  gameover := false;
  fpc := true;
  sound := false;
  readdoodler := false;
  start := true;
  snowing := false;
  monstr := false;
  monstrright := true;
  prujina := false;
  monstrleft := false;
  if game = true then begin; active := false; end;
  if gameover = true then begin; active := false; end;
  if game = false then begin; active := true; end;
  if gameover = false then begin; active := true; end;
  if settings = true then begin; active := false; end;
  if settings = false then begin; active := true; end;
  for i := 1 to sl do
  begin
    snows[i].x := random(304);
    snows[i].y := random(434);
  end;
  while active do
  
  begin
    assign(kt, 'data\databasecoins');
    reset(kt);
    for i := 1 to 21 do readln(kt, KAT[i]);
    close(kt);
    
    assign(t, 'data\database');
    reset(t);
    for i := 1 to 5 do readln(t, DB[i]);
    close(t);
    
    assign(tr, 'data\databaserecord');
    reset(tr);
    for i := 1 to 15 do readln(tr, DBR[i]);
    close(tr);
    
    
    if superhard then begin; var backgroundset1 := 'data\platformset';var backgroundset2 := DB[5];var backgroundset3 := '.png';var backgroundpicset := backgroundset1 + backgroundset2 + backgroundset3;background := Picture.Create(backgroundpicset); end else begin; var backgroundset1 := 'data\background';var backgroundset2 := DB[5];var backgroundset3 := '.png';var backgroundpicset := backgroundset1 + backgroundset2 + backgroundset3;background := Picture.Create(backgroundpicset)end;
    var backgroundsetf1 := 'data\backgroundset';
    var backgroundsetf2 := DB[5];
    var backgroundsetf3 := '.png';
    var backgroundpicset := backgroundsetf1 + backgroundsetf2 + backgroundsetf3;
    backgroundset := Picture.Create(backgroundpicset);
    
    var platforpicmset1 := 'data\platformset';
    var platforpicmset2 := DB[4];
    var platforpicmset3 := '.png';
    var platforpicmset := platforpicmset1 + platforpicmset2 + platforpicmset3; 
    platform := Picture.Create(platforpicmset);
    
    var doodlerpicset1 := 'data\doodler';
    var doodlerpicset2 := DB[3];
    var doodlerpicset3 := '.png';
    var doodlerset := doodlerpicset1 + doodlerpicset2 + doodlerpicset3; 
    doodler := Picture.Create(doodlerset);
    
    var doodlerok1picset1 := 'data\doodler';
    var doodlerok1picset2 := DB[3];
    var doodlerok1picset3 := '.png';
    var doodlerokset1 := doodlerok1picset1 + doodlerok1picset2 + doodlerok1picset3; 
    doodlerok1 := Picture.Create(doodlerokset1);
    
    var doodlerok4picset1 := 'data\doodler';
    var doodlerok4picset2 := '4' + DB[3];
    var doodlerok4picset3 := '.png';
    var doodlerokset4 := doodlerok4picset1 + doodlerok4picset2 + doodlerok4picset3; 
    doodlerok4 := Picture.Create(doodlerokset4);
    
    var doodlerok3picset1 := 'data\doodler';
    var doodlerok3picset2 := '3' + DB[3];
    var doodlerok3picset3 := '.png';
    var doodlerokset3 := doodlerok3picset1 + doodlerok3picset2 + doodlerok3picset3; 
    doodlerok3 := Picture.Create(doodlerokset3);
    
    var doodlerok5picset1 := 'data\doodler';
    var doodlerok5picset2 := '5' + DB[3];
    var doodlerok5picset3 := '.png';
    var doodlerokset5 := doodlerok5picset1 + doodlerok5picset2 + doodlerok5picset3; 
    doodlerok5 := Picture.Create(doodlerokset5);
    
    var doodlerok2picset1 := 'data\doodler';
    var doodlerok2picset2 := '2' + DB[3];
    var doodlerok2picset3 := '.png';
    var doodlerokset2 := doodlerok2picset1 + doodlerok2picset2 + doodlerok2picset3; 
    doodlerok2 := Picture.Create(doodlerokset2);
    
    
    ClearWindow;
    x := 157;
    y := 190;
    h := 200;
    settingbackground.Draw(0, 0);  
    for i := 1 to pl do
    
    begin
      platforms[i].x := random((314 - 57));
      platforms[i].y := random(444);
    end;
        //===========МЕНЮ============= 
    if snowing then begin; sym := 2;while sym < 0 do begin; sym := sym - 0.5; end;for i := 1 to sl do begin; snow.Draw(snows[i].x, snows[i].y); end;for i := 1 to sl do begin; snows[i].y := snows[i].y + 2;snows[i].x := snows[i].x - Round(sym);if snows[i].y > 434 then begin; snows[i].x := Random(304);snows[i].y := Random(50); end;if snows[i].x < -10 then begin; snows[i].x := 314end; end; end else begin; snows[i].y := Random(400); end;
    SetPenColor(clBlack);
    SetFontColor(clBlue);
    SetBrushColor(clTransparent);
    setfontsize(10);
    TextOut(105, 10, 'Doodle Jump_v1.2');
    Line(10, 30, 304, 30);
    SetPenColor(clRed);
    SetBrushColor(clYellowGreen);
    setfontsize(20);
    Rectangle(80, 50, 234, 90);
    TextOut(132, 53, 'Play');
    
    Rectangle(80, 97, 234, 137);
    TextOut(87, 100, 'How to play');
    
    Rectangle(80, 144, 234, 184);
    TextOut(117, 147, 'Setting');
    
    Rectangle(80, 191, 234, 231);
    TextOut(132, 194, 'Exit');
    
    Rectangle(64, 238, 252, 347);
    SetFontColor(clYellow);
    setfontsize(20);
    TextOut(80, 242, 'Ваш рекорд:');
    SetFontColor(clRed);
    SetFontSize(30);
    TextOut(70, 280, DB[1]);
    SetFontSize(20);
    
    SetFontColor(clBlue);
    setfontsize(10);
    SetBrushColor(clTransparent);
    TextOut(210, 426, 'Powered by Zero');
    
    SetFontColor(clYellow);
    setfontsize(10);
    SetBrushColor(clTransparent);
    TextOut(3, 426, '@aroma_kava_if');
    
    Redraw;
    
    while records do
    begin;
      ClearWindow;
      settingbackground.Draw(0, 0);
      SetPenColor(clWhiteSmoke);
      SetFontColor(clBlue);
      SetBrushColor(clTransparent);
      TextOut(106, 10, 'Doodle Jump_v1.2');
      Line(10, 30, 304, 30);
      SetBrushColor(clTransparent);
      if snowing then begin; sym := 2;while sym < 0 do begin; sym := sym - 0.5; end;for i := 1 to sl do begin; snow.Draw(snows[i].x, snows[i].y); end;for i := 1 to sl do begin; snows[i].y := snows[i].y + 2;snows[i].x := snows[i].x - Round(sym);if snows[i].y > 434 then begin; snows[i].x := Random(304);snows[i].y := Random(50); end;if snows[i].x < -10 then begin; snows[i].x := 314end; end; end;
      SetFontColor(clBlue);TextOut(34, 100, '1.');SetFontColor(clred);SetFontStyle(fsBoldItalicUnderline);TextOut(45, 100, DBR[13]);SetFontStyle(fsNormal);SetFontColor(clBlue);TextOut(130, 100, '=>');SetFontStyle(fsBoldItalicUnderline);SetFontColor(clWhiteSmoke);TextOut(170, 100, DBR[14]);
      SetFontColor(clBlue);TextOut(34, 120, '2.');SetFontColor(clred);SetFontStyle(fsBoldItalicUnderline);TextOut(45, 120, DBR[11]);SetFontStyle(fsNormal);SetFontColor(clblue);TextOut(130, 120, '=>');SetFontStyle(fsBoldItalicUnderline);SetFontColor(clWhiteSmoke);TextOut(170, 120, DBR[12]);
      SetFontColor(clblue);TextOut(34, 140, '3.');SetFontColor(clred);SetFontStyle(fsBoldItalicUnderline);TextOut(45, 140, DBR[9]);SetFontStyle(fsNormal);SetFontColor(clblue);TextOut(130, 140, '=>');SetFontStyle(fsBoldItalicUnderline);SetFontColor(clWhiteSmoke);TextOut(170, 140, DBR[10]);
      SetFontColor(clblue);TextOut(34, 160, '4.');SetFontColor(clred);SetFontStyle(fsBoldItalicUnderline);TextOut(45, 160, DBR[7]);SetFontStyle(fsNormal);SetFontColor(clblue);TextOut(130, 160, '=>');SetFontStyle(fsBoldItalicUnderline);SetFontColor(clWhiteSmoke);TextOut(170, 160, DBR[8]);
      SetFontColor(clblue);TextOut(34, 180, '5.');SetFontColor(clred);SetFontStyle(fsBoldItalicUnderline);TextOut(45, 180, DBR[5]);SetFontStyle(fsNormal);SetFontColor(clblue);TextOut(130, 180, '=>');SetFontStyle(fsBoldItalicUnderline);SetFontColor(clWhiteSmoke);TextOut(170, 180, DBR[6]);
      SetFontColor(clblue);TextOut(34, 200, '6.');SetFontColor(clred);SetFontStyle(fsBoldItalicUnderline);TextOut(45, 200, DBR[3]);SetFontStyle(fsNormal);SetFontColor(clblue);TextOut(130, 200, '=>');SetFontStyle(fsBoldItalicUnderline);SetFontColor(clWhiteSmoke);TextOut(170, 200, DBR[4]);
      SetFontColor(clblue);TextOut(34, 220, '7.');SetFontColor(clred);SetFontStyle(fsBoldItalicUnderline);TextOut(45, 220, DBR[1]);SetFontStyle(fsNormal);SetFontColor(clblue);TextOut(130, 220, '=>');SetFontStyle(fsBoldItalicUnderline);SetFontColor(clWhiteSmoke);TextOut(170, 220, DBR[2]);
      
      SetFontColor(clblue);TextOut(34, 240, 'money');SetFontColor(clred);SetFontStyle(fsBoldItalicUnderline);TextOut(145, 240, KAT[13]);
      
      SetFontColor(clRed);TextOut(6, 411, '<- Назад');SetFontStyle(fsNormal);
      Redraw;
    end;
        //===========МЕНЮ=============
    
        //=================МЕНЮ: How to play===================//
    while how do
    
    begin
      ClearWindow;
      settingbackground.Draw(0, 0);
      if snowing then begin; sym := 2;while sym < 0 do begin; sym := sym - 0.5; end;for i := 1 to sl do begin; snow.Draw(snows[i].x, snows[i].y); end;for i := 1 to sl do begin; snows[i].y := snows[i].y + 2;snows[i].x := snows[i].x - Round(sym);if snows[i].y > 434 then begin; snows[i].x := Random(304);snows[i].y := Random(50); end;if snows[i].x < -10 then begin; snows[i].x := 314end; end; end;
      SetPenColor(clBlack);
      SetFontColor(clBlue);
      SetBrushColor(clTransparent);
      TextOut(106, 10, 'Doodle Jump_v1.2');
      Line(10, 30, 304, 30);
      SetBrushColor(clTransparent);
      SetFontColor(clRed);TextOut(200, 424, 'Powered by Zero');SetFontColor(clBlue);
      TextOut(3, 50, 'Ціль гри - добратися як можно вище');
      TextOut(3, 70, 'Управління з допомогою стрілок Right і Left');
      TextOut(5, 85, 'на клавіатурі');
      TextOut(3, 105, 'Вихід в головне меню - клавіша Esc');
      SetFontColor(clRed);
      TextOut(3, 125, 'Увага!');
      SetFontColor(clBlue);
      TextOut(63, 125, 'Для збереження ваших рекордів');
      TextOut(5, 140, 'рекомендується виходити з програми з допомо -');
      TextOut(5, 155, 'гою "Exit" в гoлoвному меню');
      SetFontColor(clRed);TextOut(6, 411, '<- Назад');
      Redraw;
    end;
        //=================/МЕНЮ: How to play/===================//
        //=================МЕНЮ: Настройки===================//
    
    y3 := 44; 
    sym := 1;
    while settings do
    
    begin
      ClearWindow;
      OnKeyDown := KeyDown;
      settingbackground.Draw(0, 0);
      SetPenColor(clBlack);
      SetFontColor(clBlue);
      SetBrushColor(clTransparent);
      while sym < 0 do
      begin; sym := sym - 0.2; end;
      if snowing then
        for i := 1 to sl do begin; snow.Draw(snows[i].x, snows[i].y); end;
      for i := 1 to sl do 
      begin; snows[i].y := snows[i].y + 1;snows[i].x := snows[i].x - Round(sym);
        if snows[i].y > 434 then begin; snows[i].x := Random(304);snows[i].y := Random(50); end;if snows[i].x < -10 then begin; snows[i].x := 314end; end;
      if Left = 1 then begin; doodler := doodlerok2; end;
      if Right = 1 then begin; doodler := doodlerok1; end;
      TextOut(104, 10, 'Doodle Jump_v1.2');
      Line(10, 30, 304, 30);
      vym := vym + 0.07;
      y3 := y3 + Round(vym);
      if (y3 + 53 > 193) and (y3 + 53 < (193 + 14)) and (vym > 0) then begin; vym := -4; end;
      SetBrushColor(clTransparent);
      SetFontColor(clBlue);TextOut(5, 50, 'Ваше і`мя: ');SetFontColor(clWhite);TextOut(150, 50, DB[2]);
      SetFontColor(clBlue);TextOut(5, 170, 'Background: ');SetFontColor(clWhite);TextOut(150, 170, DB[5]);picture.create('data/backgroundset1.png');backgroundset.draw(217, 103);
      SetFontColor(clBlue);TextOut(5, 70, 'Ваш Дудлер: ');SetFontColor(clWhiteSmoke);TextOut(150, 70, 'Обычный');picture.create('data/doodler1.png');doodler.draw(220, y3);
      SetFontColor(clBlue);TextOut(5, 190, 'Platform: ');SetFontColor(clWhiteSmoke);TextOut(150, 190, DB[4]);picture.create('data/platformset1.png');platform.draw(217, 193);
      SetFontColor(clBlue);TextOut(5, 90, 'Розмір вікна: ');SetFontColor(clWhiteSmoke);TextOut(150, 90, '314x444');
      SetFontColor(clBlue);TextOut(5, 110, 'Звуки: ');SetFontColor(clWhite);TextOut(150, 110, 'вкл/викл');
      SetFontColor(clBlue);TextOut(5, 130, 'Ви знаходитесь в грі: ');SetFontColor(clWhiteSmoke);TextOut(150, 130, (round(Milliseconds / 1000)));if (round(Milliseconds / 1000) < 10) then begin; TextOut(160, 130, 'c'); end;if (round(Milliseconds / 1000) > 9) and (round(Milliseconds / 1000) < 100) then begin; TextOut(166, 130, 'c'); end;if (round(Milliseconds / 1000) < 1000) and (round(Milliseconds / 1000) > 99) then begin; TextOut(172, 130, 'c'); end;if (round(Milliseconds / 1000) < 10000) and (round(Milliseconds / 1000) > 999) then begin; TextOut(178, 130, 'c'); end;if (round(Milliseconds / 1000) < 100000) and (round(Milliseconds / 1000) > 9999) then begin; TextOut(184, 130, 'c'); end;
      SetFontColor(clBlue);TextOut(5, 150, 'Вивід fps:');SetFontColor(clWhite);TextOut(150, 150, 'вкл/викл');
      SetFontColor(clBlue);TextOut(5, 210, 'Режим "Super-Hard":');SetFontColor(clWhite);TextOut(150, 210, 'вкл/викл');
      SetFontColor(clBlue);TextOut(5, 230, 'Snowing:');SetFontColor(clWhite);TextOut(150, 230, 'вкл/викл');
      SetFontColor(clBlue);TextOut(5, 250, 'Магазин:');SetFontColor(clWhite);TextOut(150, 250, 'вкл/викл');
      SetFontColor(clRed);TextOut(5, 270, 'Powered by Zero');
      TextOut(6, 411, '<- Назад');
      SetFontColor(clRandom);TextOut(230, 411, 'coins:');TextOut(274, 411, KAT[13]);
      if readname then begin; Readln(DB[2]);readname := false; end;
      if magaz = true then
      begin;
        SetBrushColor(clYellow);Rectangle(148, 252, 173, 266);SetBrushColor(clTransparent);SetFontColor(clBlack);TextOut(150, 250, 'вкл/');
        magazz := true;
      end;
      
      while magazz = true do 
      begin;
        ClearWindow;
        settingbackground.Draw(0, 0);
        SetFontColor(clRandom);TextOut(260, 10, KAT[13]);SetFontColor(clBlue);
        
        if KAT[20] = '0' then begin; TextOut(35, 95, '200');coin.Draw(58, 95); end else begin; ok.Draw(45, 95); end;
        
        bo4ka.Draw(25, 15);if bochka = true then begin; if KAT[13].ToInteger > 200 then begin; KAT[13] := (KAT[13].ToInteger - 200).ToString; bochka := false; KAT[20] := '1' end; end;
        SetFontColor(clRed);TextOut(6, 411, '<- Назад');
        Redraw;
      end;
      
      if magaz = false then
      begin;
        SetBrushColor(clYellow);Rectangle(174, 252, 206, 266);SetBrushColor(clTransparent);SetFontColor(clBlack);TextOut(174, 250, 'викл');
      end;
      if snowing = true then
      begin;
        SetBrushColor(clYellow);Rectangle(148, 232, 173, 246);SetBrushColor(clTransparent);SetFontColor(clBlack);TextOut(150, 230, 'вкл/');
      end;
      if snowing = false then
      begin;
        SetBrushColor(clYellow);Rectangle(174, 232, 206, 246);SetBrushColor(clTransparent);SetFontColor(clBlack);TextOut(174, 230, 'викл');
      end;
      if superhard = true then
      begin;
        SetBrushColor(clYellow);Rectangle(148, 212, 173, 226);SetBrushColor(clTransparent);SetFontColor(clBlack);TextOut(150, 210, 'вкл/');
      end;
      if superhard = false then
      begin;
        SetBrushColor(clYellow);Rectangle(174, 212, 206, 226);SetBrushColor(clTransparent);SetFontColor(clBlack);TextOut(174, 210, 'викл');
      end;
      if fpc = true then
      begin;
        SetBrushColor(clYellow);Rectangle(148, 152, 173, 166);SetBrushColor(clTransparent);SetFontColor(clBlack);TextOut(150, 150, 'вкл/');
      end;
      if fpc = false then
      begin;
        SetBrushColor(clYellow);Rectangle(174, 152, 206, 166);SetBrushColor(clTransparent);SetFontColor(clBlack);TextOut(174, 150, 'викл');
      end;
      if sound = true then
      begin;
        SetBrushColor(clYellow);Rectangle(148, 112, 173, 126);SetBrushColor(clTransparent);SetFontColor(clBlack);TextOut(150, 110, 'вкл/');
      end;
      if sound = false then
      begin;
        SetBrushColor(clYellow);Rectangle(174, 112, 206, 126);SetBrushColor(clTransparent);SetFontColor(clBlack);TextOut(174, 110, 'викл');
      end;
      Redraw;
    end;
    
    while readdoodler do
    begin;
      ClearWindow;
      settingbackground.Draw(0, 0);
      if snowing then begin; sym := 1;while sym < 0 do begin; sym := sym - 0.2; end;for i := 1 to sl do begin; snow.Draw(snows[i].x, snows[i].y); end;for i := 1 to sl do begin; snows[i].y := snows[i].y + 1;snows[i].x := snows[i].x - Round(sym);if snows[i].y > 434 then begin; snows[i].x := Random(304);snows[i].y := Random(50); end;if snows[i].x < -10 then begin; snows[i].x := 314end; end; end;
      doodlerf1.Draw(3, 5); begin; if DB[3] = '1' then begin; ok.Draw(15, 61); end else begin; unok.Draw(15, 61); end; end;
      doodlerf2.Draw(63, 5); if KAT[1] = '0' then begin; TextOut((73), 61, '100'); coin.Draw(96, 61); end else begin; if DB[3] = '2' then begin; ok.Draw(78, 61); end else begin; unok.Draw(78, 61); end; end;
      doodlerf3.Draw((63 + 60), 5); if KAT[2] = '0' then begin; TextOut((73 + 60), 61, '3000'); coin.Draw((77 + 60 + 25), 61); end else begin; if DB[3] = '3' then begin; ok.Draw(140, 61); end else begin; unok.Draw(140, 61); end; end;
      doodlerf4.Draw((63 + 63 + 60), 5);  if KAT[3] = '0' then begin; TextOut((70 + 65 + 60), 61, '300'); coin.Draw((25 + 70 + 63 + 60), 61); end else begin; if DB[3] = '4' then begin; ok.Draw((186 + 11), 61); end else begin; unok.Draw(197, 61); end; end;
      doodlerf5.Draw((63 + 63 + 63 + 60), 5); if KAT[4] = '0' then begin; TextOut((70 + 65 + 65 + 60), 61, '500'); coin.Draw((25 + 70 + 63 + 65 + 60), 61); end else begin; if DB[3] = '5' then begin; ok.Draw((249 + 13), 61); end else begin; unok.Draw(262, 61); end; end;
      doodlerf6.Draw(3, 85); if KAT[5] = '0' then begin; TextOut((9), (85 + 56), '700'); coin.Draw(30, 141); end else begin; if DB[3] = '6' then begin; ok.Draw(15, 141); end else begin; unok.Draw(15, 141); end; end;
      doodlerf7.Draw(63, 85); if KAT[6] = '0' then begin; TextOut((69), (85 + 56), '1000'); coin.Draw(100, 141); end else begin; if DB[3] = '7' then begin; ok.Draw(78, 141); end else begin; unok.Draw(78, 141); end; end;
      doodlerf8.Draw(123, 85); if KAT[7] = '0' then begin; TextOut((73 + 60), (85 + 56), '2000'); coin.Draw((77 + 60 + 25), 141); end else begin; if DB[3] = '8' then begin; ok.Draw(140, 141); end else begin; unok.Draw(140, 141); end; end;
      doodlerf9.Draw(186, 85); if KAT[8] = '0' then begin; TextOut((64 + 65 + 60), (85 + 56), '2500'); coin.Draw((25 + 75 + 63 + 60), 141); end else begin; if DB[3] = '9' then begin; ok.Draw(197, 141); end else begin; unok.Draw(197, 141); end; end;
      doodlerf10.Draw(3, 162); if KAT[9] = '0' then begin; TextOut((5), (162 + 56), '1500'); coin.Draw(36, (162 + 56)); end else begin; if DB[3] = '10' then begin; ok.Draw(15, 218); end else begin; unok.Draw(15, 218); end; end;
      doodlerf11.Draw(63, 172); if KAT[10] = '0' then begin; TextOut((69), (162 + 56), '2800');  coin.Draw(100, (162 + 56)); end else begin; if DB[3] = '11' then begin; ok.Draw(85, 218); end else begin; unok.Draw(85, 218); end; end;
      doodlerf12.Draw(123, 162); if KAT[11] = '0' then begin; TextOut((73 + 60), (162 + 56), '2700'); coin.Draw((77 + 60 + 25), (162 + 56)); end else begin; if DB[3] = '12' then begin; ok.Draw(143, 218); end else begin; unok.Draw(143, 218); end; end;
      if rd1 then begin; DB[3] := '1';rd1 := false;settings := false;readdoodler := false;active := true; end;
      if rd2 then begin; if KAT[1] = '0' then begin; if KAT[13].ToInteger > 100 then begin; DB[3] := '2';rd2 := false;settings := false;readdoodler := false;active := true; KAT[13] := (KAT[13].ToInteger - 100).ToString; KAT[1] := '1'; end else begin; ClearWindow; background.Draw(0, 0); TextOut(75, 200, 'У вас недостатньо коштів'); if (Round(Milliseconds / 1000) mod 5 = 0) then begin; rd2 := false; end; end; end else begin; DB[3] := '2';rd2 := false;settings := false;readdoodler := false;active := true; end; end;
      if rd3 then begin; if KAT[2] = '0' then begin; if KAT[13].ToInteger > 3000 then begin; DB[3] := '3';rd3 := false;settings := false;readdoodler := false;active := true; KAT[13] := (KAT[13].ToInteger - 3000).ToString; KAT[2] := '1'; end else begin; ClearWindow; background.Draw(0, 0); TextOut(75, 200, 'У вас недостатньо коштів'); if (Round(Milliseconds / 1000) mod 5 = 0) then begin; rd3 := false; end; end; end else begin; DB[3] := '3';rd3 := false;settings := false;readdoodler := false;active := true; end; end;
      if rd4 then begin; if KAT[3] = '0' then begin; if KAT[13].ToInteger > 300 then begin; DB[3] := '4';rd4 := false;settings := false;readdoodler := false;active := true; KAT[13] := (KAT[13].ToInteger - 300).ToString; KAT[3] := '1'; end else begin; ClearWindow; background.Draw(0, 0); TextOut(75, 200, 'У вас недостатньо коштів'); if (Round(Milliseconds / 1000) mod 5 = 0) then begin; rd4 := false; end; end; end else begin; DB[3] := '4';rd4 := false;settings := false;readdoodler := false;active := true; end; end;
      if rd5 then begin; if KAT[4] = '0' then begin; if KAT[13].ToInteger > 500 then begin; DB[3] := '5';rd5 := false;settings := false;readdoodler := false;active := true; KAT[13] := (KAT[13].ToInteger - 500).ToString; KAT[4] := '1'; end else begin; ClearWindow; background.Draw(0, 0); TextOut(75, 200, 'У вас недостатньо коштів'); if (Round(Milliseconds / 1000) mod 5 = 0) then begin; rd5 := false; end; end; end else begin; DB[3] := '5';rd5 := false;settings := false;readdoodler := false;active := true; end; end;
      if rd6 then begin; if KAT[5] = '0' then begin; if KAT[13].ToInteger > 700 then begin; DB[3] := '6';rd6 := false;settings := false;readdoodler := false;active := true; KAT[13] := (KAT[13].ToInteger - 700).ToString; KAT[5] := '1'; end else begin; ClearWindow; background.Draw(0, 0); TextOut(75, 200, 'У вас недостатньо коштів'); if (Round(Milliseconds / 1000) mod 5 = 0) then begin; rd6 := false; end; end; end else begin; DB[3] := '6';rd6 := false;settings := false;readdoodler := false;active := true; end; end;
      if rd7 then begin; if KAT[6] = '0' then begin; if KAT[13].ToInteger > 1000 then begin; DB[3] := '7';rd7 := false;settings := false;readdoodler := false;active := true; KAT[13] := (KAT[13].ToInteger - 1000).ToString; KAT[6] := '1'; end else begin; ClearWindow; background.Draw(0, 0); TextOut(75, 200, 'У вас недостатньо коштів'); if (Round(Milliseconds / 1000) mod 5 = 0) then begin; rd7 := false; end; end; end else begin; DB[3] := '7';rd7 := false;settings := false;readdoodler := false;active := true; end; end;
      if rd8 then begin; if KAT[7] = '0' then begin; if KAT[13].ToInteger > 2000 then begin; DB[3] := '8';rd8 := false;settings := false;readdoodler := false;active := true; KAT[13] := (KAT[13].ToInteger - 2000).ToString; KAT[7] := '1'; end else begin; ClearWindow; background.Draw(0, 0); TextOut(75, 200, 'У вас недостатньо коштів'); if (Round(Milliseconds / 1000) mod 5 = 0) then begin; rd8 := false; end; end; end else begin; DB[3] := '8';rd8 := false;settings := false;readdoodler := false;active := true; end; end;
      if rd9 then begin; if KAT[8] = '0' then begin; if KAT[13].ToInteger > 2500 then begin; DB[3] := '9';rd9 := false;settings := false;readdoodler := false;active := true; KAT[13] := (KAT[13].ToInteger - 2500).ToString; KAT[8] := '1'; end else begin; ClearWindow; background.Draw(0, 0); TextOut(75, 200, 'У вас недостатньо коштів'); if (Round(Milliseconds / 1000) mod 5 = 0) then begin; rd9 := false; end; end; end else begin; DB[3] := '9';rd9 := false;settings := false;readdoodler := false;active := true; end; end;
      if rd10 then begin; if KAT[9] = '0' then begin; if KAT[13].ToInteger > 1500 then begin; DB[3] := '10';rd10 := false;settings := false;readdoodler := false;active := true; KAT[13] := (KAT[13].ToInteger - 1050).ToString; KAT[9] := '1'; end else begin; ClearWindow; background.Draw(0, 0); TextOut(75, 200, 'У вас недостатньо коштів'); if (Round(Milliseconds / 1000) mod 5 = 0) then begin; rd10 := false; end; end; end else begin; DB[3] := '10';rd10 := false;settings := false;readdoodler := false;active := true; end; end;
      if rd11 then begin; if KAT[10] = '0' then begin; if KAT[13].ToInteger > 2800 then begin; DB[3] := '11';rd11 := false;settings := false;readdoodler := false;active := true; KAT[13] := (KAT[13].ToInteger - 2800).ToString; KAT[10] := '1'; end else begin; ClearWindow; background.Draw(0, 0); TextOut(75, 200, 'У вас недостатньо коштів'); if (Round(Milliseconds / 1000) mod 5 = 0) then begin; rd11 := false; end; end; end else begin; DB[3] := '11';rd11 := false;settings := false;readdoodler := false;active := true; end; end;
      if rd12 then begin; if KAT[11] = '0' then begin; if KAT[13].ToInteger > 2700 then begin; DB[3] := '12';rd12 := false;settings := false;readdoodler := false;active := true; KAT[13] := (KAT[13].ToInteger - 2700).ToString; KAT[11] := '1'; end else begin; ClearWindow; background.Draw(0, 0); TextOut(75, 200, 'У вас недостатньо коштів'); if (Round(Milliseconds / 1000) mod 5 = 0) then begin; rd12 := false; end; end; end else begin; DB[3] := '12';rd12 := false;settings := false;readdoodler := false;active := true; end; end;
      SetFontColor(clRed);TextOut(6, 411, '<- Назад');
      SetFontColor(clRandom);TextOut(230, 411, 'coins:');TextOut(274, 411, KAT[13]);
      Redraw;
    end;
    
    while readplatform do
    begin;
      ClearWindow;
      settingbackground.Draw(0, 0);
      if snowing then begin; sym := 1;while sym < 0 do begin; sym := sym - 0.2; end;for i := 1 to sl do begin; snow.Draw(snows[i].x, snows[i].y); end;for i := 1 to sl do begin; snows[i].y := snows[i].y + 1;snows[i].x := snows[i].x - Round(sym);if snows[i].y > 434 then begin; snows[i].x := Random(304);snows[i].y := Random(50); end;if snows[i].x < -10 then begin; snows[i].x := 314end; end; end;
      platformf1.Draw(3, 10);
      platformf2.Draw(63, 10);
      platformf3.Draw((63 + 60), 10);
      platformf4.Draw((63 + 63 + 60), 10);
      platformf5.Draw((63 + 63 + 63 + 60), 10);
      platformf6.Draw(3, 30);
      platformf7.Draw(63, 30);
      platformf8.Draw(123, 30);
      if rp1 then begin; DB[4] := '1';rp1 := false;settings := false;readplatform := false;active := true; end;
      if rp2 then begin; DB[4] := '2';rp2 := false;settings := false;readplatform := false;active := true; end;
      if rp3 then begin; DB[4] := '3';rp3 := false;settings := false;readplatform := false;active := true; end;
      if rp4 then begin; DB[4] := '4';rp4 := false;settings := false;readplatform := false;active := true; end;
      if rp5 then begin; DB[4] := '5';rp5 := false;settings := false;readplatform := false;active := true; end;
      if rp6 then begin; DB[4] := '6';rp6 := false;settings := false;readplatform := false;active := true; end;
      if rp7 then begin; DB[4] := '7';rp7 := false;settings := false;readplatform := false;active := true; end;
      if rp8 then begin; DB[4] := '8';rp8 := false;settings := false;readplatform := false;active := true; end;
      SetFontColor(clRed);TextOut(6, 411, '<- Назад');
      Redraw;
    end;
    while readbackground do
    begin;
      ClearWindow;
      settingbackground.Draw(0, 0);
      if snowing then begin; sym := 1;while sym < 0 do begin; sym := sym - 0.2; end;for i := 1 to sl do begin; snow.Draw(snows[i].x, snows[i].y); end;for i := 1 to sl do begin; snows[i].y := snows[i].y + 1;snows[i].x := snows[i].x - Round(sym);if snows[i].y > 434 then begin; snows[i].x := Random(304);snows[i].y := Random(50); end;if snows[i].x < -10 then begin; snows[i].x := 314end; end; end;
      backgroundfm1.Draw(3, 10); if KAT[14] = '0' then begin; TextOut(17, 95, '100'); coin.Draw(40, 95); end else begin; if DB[5] = '1' then begin; ok.Draw(21, 95) end else begin; unok.Draw(21, 95); end; end;
      backgroundfm2.Draw(63, 10); if KAT[15] = '0' then begin; TextOut(77, 95, '1000'); coin.Draw(106, 96); end else begin; if DB[5] = '2' then begin; ok.Draw(82, 95) end else begin; unok.Draw(82, 95); end; end;
      backgroundfm3.Draw((63 + 60), 10); if DB[5] = '3' then begin; ok.Draw(141, 95) end else begin; unok.Draw(141, 95); end;
      backgroundfm4.Draw((63 + 63 + 60), 10); if KAT[16] = '0' then begin; TextOut(197, 95, '1500'); coin.Draw(226, 96); end else begin; if DB[5] = '4' then begin; ok.Draw(205, 95) end else begin; unok.Draw(205, 95); end; end;
      backgroundfm5.Draw((63 + 63 + 63 + 60), 10); if KAT[17] = '0' then begin; TextOut(257, 95, '2000'); coin.Draw(286, 96); end else begin; if DB[5] = '5' then begin; ok.Draw(269, 95) end else begin; unok.Draw(269, 95); end; end;
      backgroundfm6.Draw(3, 115); if KAT[18] = '0' then begin; TextOut(11, 200, '3000'); coin.Draw(43, 200); end else begin; if DB[5] = '6' then begin; ok.Draw(21, 200) end else begin; unok.Draw(21, 200); end; end;
      backgroundfm7.Draw(63, 115); if Kat[19] = '0' then begin; TextOut(77, 200, '5000'); coin.Draw(106, 200); end else begin; if DB[5] = '7' then begin; ok.Draw(82, 200) end else begin; unok.Draw(82, 200); end; end;
      
      if rb1 then begin; if KAT[14] = '0' then begin; if KAT[13].ToInteger > 100 then begin; DB[5] := '1';rb1 := false;settings := false;readdoodler := false;active := true; game := false; KAT[13] := (KAT[13].ToInteger - 100).ToString; KAT[14] := '1'; end else begin; ClearWindow; background.Draw(0, 0); TextOut(75, 200, 'У вас недостатньо коштів'); if (Round(Milliseconds / 1000) mod 5 = 0) then begin; rb1 := false; end; end; end else begin; DB[5] := '1';rb1 := false;settings := false;readdoodler := false;active := true; end; end;
      if rb2 then begin; if KAT[15] = '0' then begin; if KAT[13].ToInteger > 1000 then begin; DB[5] := '2';rb2 := false;settings := false;readbackground := false;active := true; game := false; KAT[13] := (KAT[13].ToInteger - 1000).ToString; KAT[15] := '1' end else begin; ClearWindow; background.Draw(0, 0); TextOut(75, 200, 'У вас недостатньо коштів'); if (Round(Milliseconds / 1000) mod 5 = 0) then begin; rb2 := false; end; end; end else begin; DB[5] := '2';rb2 := false;settings := false;readdoodler := false;active := true; end; end;
      if rb3 then begin; DB[5] := '3';rb3 := false;settings := false;readbackground := false;active := true; game := false; end;
      if rb4 then begin; if KAT[16] = '0' then begin; if KAT[13].ToInteger > 1500 then begin; DB[5] := '4';rb4 := false;settings := false;readbackground := false;active := true; game := false; KAT[13] := (KAT[13].ToInteger - 1500).ToString; KAT[16] := '1' end else begin; ClearWindow; background.Draw(0, 0); TextOut(75, 200, 'У вас недостатньо коштів'); if (Round(Milliseconds / 1000) mod 5 = 0) then begin; rb4 := false; end; end; end else begin; DB[5] := '4';rb4 := false;settings := false;readdoodler := false;active := true; end; end;
      if rb5 then begin; if KAT[17] = '0' then begin; if KAT[13].ToInteger > 2000 then begin; DB[5] := '5';rb5 := false;settings := false;readbackground := false;active := true; game := false; KAT[13] := (KAT[13].ToInteger - 2000).ToString; KAT[17] := '1' end else begin; ClearWindow; background.Draw(0, 0); TextOut(75, 200, 'У вас недостатньо коштів'); if (Round(Milliseconds / 1000) mod 5 = 0) then begin; rb5 := false; end; end; end else begin; DB[5] := '5';rb5 := false;settings := false;readdoodler := false;active := true; end; end;
      if rb6 then begin; if KAT[18] = '0' then begin; if KAT[13].ToInteger > 3000 then begin; DB[5] := '6';rb6 := false;settings := false;readbackground := false;active := true; game := false; KAT[13] := (KAT[13].ToInteger - 3000).ToString; KAT[18] := '1' end else begin; ClearWindow; background.Draw(0, 0); TextOut(75, 200, 'У вас недостатньо коштів'); if (Round(Milliseconds / 1000) mod 5 = 0) then begin; rb6 := false; end; end; end else begin; DB[5] := '6';rb6 := false;settings := false;readdoodler := false;active := true; end; end;
      if rb7 then begin; if KAT[19] = '0' then begin; if KAT[13].ToInteger > 5000 then begin; DB[5] := '7';rb7 := false;settings := false;readbackground := false;active := true; game := false; KAT[13] := (KAT[13].ToInteger - 5000).ToString; KAT[19] := '1' end else begin; ClearWindow; background.Draw(0, 0); TextOut(75, 200, 'У вас недостатньо коштів'); if (Round(Milliseconds / 1000) mod 5 = 0) then begin; rb7 := false; end; end; end else begin; DB[5] := '7';rb7 := false;settings := false;readdoodler := false;active := true; end; end;
      SetFontColor(clRed);TextOut(6, 411, '<- Назад');
      SetFontColor(clRandom);TextOut(230, 411, 'coins:');TextOut(274, 411, KAT[13]);
      Redraw;
    end;
    
        //=================/МЕНЮ: Настройки===================//
    score := 0;
    cs := 0;
    var x5 := 277;
    var y5 := 0;
    
    var y6 := 16;
    var y7 := 0;
    var y8 := 16;
    var y9 := 0;
    var y10 := 16;
    var y11 := 0;
    var y12 := 16;
    var y13 := 0;
    var y14 := 16;
    var y15 := 0;
    var y16 := 16;
    var y17 := 0;
    var max := platforms[1].y;
    var imax := 1;
    var xz := 17;
    var izx := imax;
    var ym := -50;
    x4 := 270;y4 := 16;
    var xk := x + 28;
    var yk := y;
    sec := false;
    for i := 1 to 14 do begin; if max > platforms[i].y then begin; max := platforms[i].y;imax := i; end; end;
    var zx := max;
    while game do
    begin
      SetFontColor(clWhiteSmoke);SetFontStyle(fsBoldItalicUnderline);SetFontSize(14);
      background.Draw(0, 0);
      if KAT[20] = '1' then begin vy := -35; score := 3001; KAT[20] := '0'; end;
      if ((score > 1300) and (score < 1430)) or ((score > 2300) and (score < 2430)) or ((score > 3300) and (score < 3430)) or ((score > 4300) and (score < 4430)) or ((score > 5300) and (score < 5430)) or ((score > 6300) and (score < 6430)) or ((score > 7300) and (score < 7430)) or ((score > 8300) and (score < 8430)) or ((score > 9300) and (score < 9430)) or ((score > 10300) and (score < 10430)) or ((score > 11300) and (score < 11430)) or ((score > 12300) and (score < 12430)) then begin; racketka := true; end else racketka := false; 
      begin;
        if racketka = true then begin; raceta.Draw((platforms[i].x + 6), (platforms[i].y - 45)); end;
        if ((platforms[i].y - 45) > 420) then racketka := false;
      end;
      if (racketka = true) and (x + 36 > (platforms[i].x + 6)) and (x + 5 < (platforms[i].x + 56)) and (y + 53 > (platforms[i].y - 45)) and (y + 53 < platforms[i].y) then
      begin;
        racketa := true;
        vy := -24;
      end;
      
      if vy > 0 then begin; racketa := false; end;
      
      if (prujina = true) and (racketka = false) then begin; pryjina.Draw((platforms[i].x + 5), (platforms[i].y - 13)); end;
      if (x + 36 > (platforms[i].x + 4)) and (x + 5 < (platforms[i].x + 23)) and (y + 53 > (platforms[i].y - 11)) and (y + 53 < platforms[i].y  - 6) and (vy > 0) then 
      begin
        begin;
          System.Console.Beep(1000, 80); 
        end;
        vy := -9;
      end; 
      
      if (coins = true) then begin; coin.Draw(platforms[i - 4].x + 15, platforms[i - 4].y - 16) end;
      if (x + 36 > (platforms[i - 4].x + 15)) and (x + 5 < (platforms[i - 4].x + 30)) and (y + 53 > (platforms[i - 4].y - 16)) and (y + 53 < platforms[i - 4].y  - 1) and (coins = true) then 
      begin;
        cs := cs + 1;
        coins := false;
      end;
      
      if attack = false then war := true;
      if attack = false then xk := (x + 28);
      if attack = false then yk := y;for i := 1 to 5 do 
      begin; if attack then begin; setbrushcolor(clYellow);setfontcolor(clYellow);Circle(xk, yk, 7);setbrushcolor(clTransparent);setfontcolor(clTransparent);
          yk := yk - 1; doodler := doodlerok3;
          if yk < 0 then begin; attack := false;doodler := doodlerok1; end; end; end;
      if war = true then begin; if attack then begin; if (xk > (xz)) and (xk < (xz + 57)) and (yk > ym) and (yk < ym + 57) then begin; war := false;monstr1 := false;monstr2 := false;monstr3 := false;ym := -57;attack := false; doodler := doodlerok1; end; end; end;
      
      if (score mod 1000 = 0) and (score <> 0) then begin; monstr1 := true; end;
      if (score mod 2000 = 0) and (score <> 0) then begin; monstr2 := true;monstr1 := false; end;
      if (score mod 3000 = 0) and (score <> 0) then begin; monstr3 := true;monstr1 := false;monstr2 := false; end;
      if ym > 444 then begin; monstr1 := false; monstr2 := false; monstr3 := false;ym := -50; end;
      monstr := monstr1;
      
      if snowing then begin; begin; sym := 1;while sym < 0 do begin; sym := sym - 0.2; end;for i := 1 to sl do begin; snow.Draw(snows[i].x, snows[i].y); end;for i := 1 to sl do begin; snows[i].y := snows[i].y + 1;snows[i].x := snows[i].x - Round(sym);if snows[i].y > 434 then begin; snows[i].x := Random(304);snows[i].y := Random(50); end;if snows[i].x < -10 then begin; snows[i].x := 314end; end; end; end;
      for i := 1 to pl do platform.Draw(platforms[i].x, platforms[i].y); 
      if monstr1 then begin; monster1.Draw(xz, ym); end;
      if monstr2 then begin; monster2.Draw(xz, ym); end;
      if monstr3 then begin; monster3.Draw(xz, ym); end;
      doodler.Draw(x, y);
      if (active = true) then 
      begin;
        if (score > (sc1 - 100)) and (score < sc2) and (score < sc3) and (score < sc4) and (score < sc5) and (score < sc6) and (score < sc7) then begin; TextOut(x5, y5, DBR[1]);poloska1.Draw(x4, y4);if y = (h) then begin; y4 := y4 - round(vy);y5 := y5 - round(vy); end; end;
        if (score > (sc2 - 100)) and (score < sc3) and (score < sc4) and (score < sc5) and (score < sc6) and (score < sc7) then begin; TextOut(x5, y7, DBR[3]);poloska2.Draw(x4, y6);if y = (h) then begin; y6 := y6 - round(vy);y7 := y7 - round(vy); end; end;
        if (score > (sc3 - 100)) and (score < sc4) and (score < sc5) and (score < sc6) and (score < sc7) then begin; TextOut(x5, y9, DBR[5]);poloska3.Draw(x4, y8);if y = (h) then begin; y8 := y8 - round(vy);y9 := y9 - round(vy); end; end;
        if (score > (sc4 - 100)) and (score < sc5) and (score < sc6) and (score < sc7) then begin; TextOut(x5, y11, DBR[7]);poloska4.Draw(x4, y10);if y = (h) then begin; y10 := y10 - round(vy);y11 := y11 - round(vy); end; end;
        if (score > (sc5 - 100)) and (score < sc6) and (score < sc7) then begin; TextOut(x5, y13, DBR[9]);poloska5.Draw(x4, y12);if y = (h) then begin; y12 := y12 - round(vy);y13 := y13 - round(vy); end; end;
        if (score > (sc6 - 100)) and (score < sc7) then begin; TextOut(x5, y15, DBR[11]);poloska6.Draw(x4, y14);if y = (h) then begin; y14 := y14 - round(vy);y15 := y15 - round(vy); end; end;
        if (score > (sc7 - 100)) then begin; TextOut(x5, y17, DBR[13]);poloska7.Draw(x4, y16);if y = (h) then begin; y16 := y16 - round(vy);y17 := y17 - round(vy); end; end;
      end;
      SetFontColor(clBlack);TextOut(3, 6, score.ToString);SetFontColor(clRed);
      SetFontColor(clBlack);TextOut(285, 6, cs.ToString);SetFontColor(clRed);
      val(DB[1], sc, err);
      val(DBR[2], sc1, err1);
      val(DBR[4], sc2, err2);
      val(DBR[6], sc3, err3);
      val(DBR[8], sc4, err4);
      val(DBR[10], sc5, err5);
      val(DBR[12], sc6, err6);
      val(DBR[14], sc7, err7);
      SetBrushColor(clTransparent);
      SetPenColor(clRed);
      
      fps := round(MillisecondsDelta * 10);
      if fpc then begin; TextOut(295, 424, fps); end; 
      if score > sc then TextOut(1, 24, 'New record!');
      
      if Left = 1 then begin; x := x - 3;if racketa = false then begin; doodler := doodlerok2; end; if racketa = true then begin; doodler := doodlerok5; end; end;
      if Right = 1 then begin; x := x + 3;if racketa = false then begin; doodler := doodlerok1; end; if racketa = true then begin; doodler := doodlerok4; end; end;
      
      if x > (314 + 58) then x := -57;
      if x < -58 then x := (314 + 57);
      if xz = 17 then begin; monstrright := true;monstrleft := false; end;
      if xz = (314 - 57 - 17)  then begin; monstrleft := true;monstrright := false; end;
      if monstrright  then begin; xz := xz + 1; end; 
      if monstrleft  then begin; xz := xz - 1; end;
      vy := vy + 0.1;
      y := y + round(vy);
      if y < h then begin; if (monstr1 = true) or (monstr2 = true) or (monstr3 = true) then begin; ym := ym - round(vy); end; end;
      if (monstr1 = true) or (monstr2 = true) or (monstr3 = true) then begin; if (x + 36 > xz) and (x + 5 < (xz + 56)) and (y + 53 > (ym + 4)) and (y + 53 < (ym + 56)) then 
        begin;
          gameover := true;
          game := false;
        end; end;
      
      if (score mod 400 = 0) then begin; prujina := true; end;
      if (score mod 150 = 0) then begin; coins := true; end;
      
      if monstr1 = true then begin
        if (x + 36 > xz) and (x + 5 < (xz + 57)) and (y + 53 > ym) and (y + 53 < (ym + 4)) and (vy > 0) then
        begin
          if sound = true then 
          begin;
            System.Console.Beep(1000, 80); 
          end;
          vy := -10;
        end; end;
      
      if monstr2 = true then begin
        if (x + 36 > xz) and (x + 5 < (xz + 57)) and (y + 53 > ym) and (y + 53 < (ym + 4)) and (vy > 0) then
        begin
          if sound = true then 
          begin;
            System.Console.Beep(1000, 80); 
          end;
          vy := -10;
        end; end;
      
      if monstr3 = true then begin
        if (x + 36 > xz) and (x + 5 < (xz + 49)) and (y + 53 > ym) and (y + 53 < (ym + 4)) and (vy > 0) then
        begin
          if sound = true then 
          begin;
            System.Console.Beep(1000, 80); 
          end;
          vy := -10;
        end; end;
      
      for i := 1 to pl do
        if (x + 36 > platforms[i].x) and (x + 5 < platforms[i].x + 56) and (y + 53 > platforms[i].y) and (y + 53 < platforms[i].y + 14) and (vy > 0) then begin
          if sound = true then begin; system.Console.Beep(400, 80); end; vy := -6; end;
      if y < h then
      begin;
        if snowing then begin; for i := 1 to sl do begin; snows[i].y := snows[i].y - round(-2); end; end;
        for i := 1 to pl do
        begin
          y := h;
          begin; snows[i].y := snows[i].y - round(vy); end;
          platforms[i].y := platforms[i].y - round(vy);
          if platforms[i].y > 444 then
          begin
            platforms[i].y := Random(50);
            platforms[i].x := random((314 - 57));
            if score = 1000 then begin; end;
          end;
        end;   
        score := score + 1;
      end;  
      
      
      
      
      if y > 397 then
      begin
        y := 385;
        gameover := true;
        game := false;
      end;
      SetFontSize(10);SetFontStyle(fsNormal);SetBrushColor(clTransparent);
      Redraw;
    end;
    x1 := 500;
    while gameover do
    begin
      y := 385;
      monstr := false;
      monstr1 := false;
      monstr2 := false;
      monstr3 := false;
      ClearWindow;
      if superhard then begin; settingbackground.Draw(0, 0); end else begin; background.Draw(0, 0); end;
      if snowing then begin; begin; sym := 1;while sym < 0 do begin; sym := sym - 0.2; end;for i := 1 to sl do begin; snow.Draw(snows[i].x, snows[i].y); end;for i := 1 to sl do begin; snows[i].y := snows[i].y + 1;snows[i].x := snows[i].x - Round(sym);if snows[i].y > 434 then begin; snows[i].x := Random(304);snows[i].y := Random(50); end;if snows[i].x < -10 then begin; snows[i].x := 314end; end; end; end;
      for i := 1 to pl do 
      begin; platform.Draw(platforms[i].x, platforms[i].y);
        platforms[i].y := platforms[i].y - 6; end;
      doodler.Draw(x, y);
      restart := Picture.Create('data\restart.png');
      while (x1 > 294) do
      begin; x1 := (x1 - 1);restart.Draw(126, x1); end;
      SetBrushColor(clYellow);Rectangle(122, 292, (128 + 96), (297 + 45));restart.Draw(126, x1);
      
      Rectangle(117, 270, (122 + 5 + 96), 190);SetFontSize(14);TextOut(126, 194, 'You score:');TextOut(150, 234, score);if sc > score then SetFontSize(1);SetBrushColor(clTransparent);TextOut(124, 213, 'New recort');
      Redraw;
    end;
    
    if score > sc then DB[1] := score.ToString;
    if (score > sc1) and (score < sc2) and (score < sc3) and (score < sc4) and (score < sc5) and (score < sc6) and (score < sc7) then begin; DBR[2] := score.ToString;DBR[1] := DB[2]; end;
    if (score > sc2) and (score < sc3) and (score < sc4) and (score < sc5) and (score < sc6) and (score < sc7) then begin; DBR[4] := score.ToString;DBR[3] := DB[2]; end;
    if (score > sc3) and (score < sc4) and (score < sc5) and (score < sc6) and (score < sc7) then begin; DBR[6] := score.ToString;DBR[5] := DB[2]; end;
    if (score > sc4) and (score < sc5) and (score < sc6) and (score < sc7) then begin; DBR[8] := score.ToString;DBR[7] := DB[2]; end;
    if (score > sc5) and (score < sc6) and (score < sc7) then begin; DBR[10] := score.ToString;DBR[9] := DB[2]; end;
    if (score > sc6) and (score < sc7) then begin; DBR[12] := score.ToString;DBR[11] := DB[2]; end;
    if score > sc7 then begin; DBR[14] := score.ToString;DBR[13] := DB[2]; end;
    DB[1] := DBR[14];
    valik := KAT[13].ToInteger;
    cs2 :=  valik + cs;
    KAT[13] := cs2.ToString;
    assign(t, 'data\database');
    rewrite(t);
    for i := 1 to 5 do writeln(t, DB[i]);close(t);
    
    assign(tr, 'data\databaserecord');
    rewrite(tr);
    for i := 1 to 15 do writeln(tr, DBR[i]);close(tr);
    
    assign(kt, 'data\databasecoins');
    Rewrite(kt);
    for i := 1 to 21 do writeln(kt, KAT[i]);
    close(kt);
    
  end;
  Halt;
end.