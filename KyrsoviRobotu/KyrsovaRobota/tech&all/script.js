$(document).ready(function(e){
	//Пересування верхньої полоски
	$("#m1").on("click", function(){
		$(".nav-active").animate({marginLeft: '525px', width: '147px'}, 1000);
	});
	$("#m2").on("click", function(){
		$(".nav-active").animate({marginLeft: '675px', width: '75px'}, 1000);
	});
	$("#m3").on("click", function(){
		$(".nav-active").animate({marginLeft: '745px', width: '137px'}, 1000);
	});
	$("#m4").on("click", function(){
		$(".nav-active").animate({marginLeft: '880px', width: '45px'}, 1000);
	});
	$("#m5").on("click", function(){
		$(".nav-active").animate({marginLeft: '925px', width: '87px'}, 1000);
	});
	$("#m6").on("click", function(){
		$(".nav-active").animate({marginLeft: '1010px', width: '115px'}, 1000);
	});

	let slide = 1;
	//Пересування та вирахування кнопок слайдера
	$(".arrow-left").on("click", function(){
		slide--;
		$(".computer").slideToggle();
		$(".computer").slideToggle();
			if(slide > 3){
				slide = 1;
			}
			if (slide < 1) {
				slide = 3;
			}
		$(`.slide${slide}`).animate({opacity: '1'}, 100);
		$(`.slide${slide+1}`).animate({opacity: '0.7'}, 100);
		$(`.slide${slide+2}`).animate({opacity: '0.7'}, 100);
		$(`.slide${slide-1}`).animate({opacity: '0.7'}, 100);
		$(`.slide${slide-2}`).animate({opacity: '0.7'}, 100);
	});
	$(".arrow-right").on("click", function(){
		slide++;
		$(".computer").slideToggle();
		$(".computer").slideToggle();
			if(slide > 3){
				slide = 1;
			}
			if (slide < 1) {
				slide = 3;
			}
		$(`.slide${slide}`).animate({opacity: '1'}, 100);
		$(`.slide${slide+1}`).animate({opacity: '0.7'}, 100);
		$(`.slide${slide+2}`).animate({opacity: '0.7'}, 100);
		$(`.slide${slide-1}`).animate({opacity: '0.7'}, 100);
		$(`.slide${slide-2}`).animate({opacity: '0.7'}, 100);
	});

	$("#btn1").on("click", function(){
	//Нажимання на кнопку GO!
		$(this).animate({marginLeft: '100px', marginTop: '42px', width: '200px', height: '50px'}, 100);
		setTimeout(() => { $(this).animate({marginLeft: '110px', marginTop: '46px', width: '181px', height: '42px'}, 100); }, 400);
	});


	$("#prod-1").on({
	//Утримання пульсуючої картинки 1
		mouseenter: function(){
      		document.getElementById("prod-1").style.animation = "none";
		},
		mouseleave: function(){
      		document.getElementById("prod-1").style.animation = "radial-pulse 1s infinite";
		}
	});
	$("#prod-2").on({
	//Утримання пульсуючої картинки 2
		mouseenter: function(){
      		document.getElementById("prod-2").style.animation = "none";
		},
		mouseleave: function(){
      		document.getElementById("prod-2").style.animation = "radial-pulse 1s infinite";
		}
	});
	$("#prod-3").on({
	//Утримання пульсуючої картинки 3
		mouseenter: function(){
      		document.getElementById("prod-3").style.animation = "none";
		},
		mouseleave: function(){
      		document.getElementById("prod-3").style.animation = "radial-pulse 1s infinite";
		}
	});
	$("#prod-4").on({
	//Утримання пульсуючої картинки 4
		mouseenter: function(){
      		document.getElementById("prod-4").style.animation = "none";
		},
		mouseleave: function(){
      		document.getElementById("prod-4").style.animation = "radial-pulse 1s infinite";
		}
	});

	$("#clients").on({
	//Збільшення чіткості забраження clients
		mouseenter: function(){
			$(this).animate({opacity: '1'}, 100);
		},
		mouseleave: function(){
			$(this).animate({opacity: '0.5'}, 100);
		}
	});
});